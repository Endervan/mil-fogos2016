-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: milfogos2016.mysql.dbaas.com.br
-- Generation Time: 19-Jul-2016 às 16:47
-- Versão do servidor: 5.6.30-76.3-log
-- PHP Version: 5.6.22-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `milfogos2016`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_avaliacoes_produtos`
--

CREATE TABLE `tb_avaliacoes_produtos` (
  `idavaliacaoproduto` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `nota` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners`
--

CREATE TABLE `tb_banners` (
  `idbanner` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `legenda` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_btn_orcamento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_banners`
--

INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `ativo`, `ordem`, `tipo_banner`, `url_amigavel`, `url`, `legenda`, `url_btn_orcamento`) VALUES
(1, 'Mobile Banner 1', '3103201603181362124252..jpg', 'SIM', NULL, '2', 'mobile-banner-1', '/mobile/produtos', 'REALIZE SEU MAIOR SONHO COM SUCESSO E SEGURANÇA', NULL),
(2, 'Mobile Banner 2', '3103201603211347880843..jpg', 'SIM', NULL, '2', 'mobile-banner-2', '/mobile/produtos', 'DEIXE SUA FESTA AINDA MAIS INESQUECÍVEL', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners_internas`
--

CREATE TABLE `tb_banners_internas` (
  `idbannerinterna` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `legenda` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_banners_internas`
--

INSERT INTO `tb_banners_internas` (`idbannerinterna`, `titulo`, `imagem`, `ordem`, `url_amigavel`, `ativo`, `legenda`) VALUES
(1, 'Portfólios', '2603201604071297553961.jpg', NULL, 'portfolio', 'SIM', 'CONFIRA NOSSOS TRABALHOS'),
(2, 'Portfólio Dentro', '2603201604281142424974.jpg', NULL, 'portfolio-dentro', 'SIM', 'CONFIRA NOSSOS TRABALHOS'),
(3, 'Dicas', '2803201609541224139199.jpg', NULL, 'dicas', 'SIM', 'CONFIRA NOSSAS DICAS'),
(4, 'Dica Dentro', '2803201610071235362756.jpg', NULL, 'dica-dentro', 'SIM', 'CONFIRA NOSSAS DICAS'),
(5, 'Fale Conosco', '2803201610541157695219.jpg', NULL, 'fale-conosco', 'SIM', 'ENTRE EM CONTATO CONOSCO'),
(6, 'Trabalhe Conosco', '2903201612261401526509.jpg', NULL, 'trabalhe-conosco', 'SIM', 'TRABALHE CONOSCO'),
(7, 'Produtos', '2903201612451308923198.jpg', NULL, 'produtos', 'SIM', 'CONHEÇA NOSSOS PRODUTOS'),
(8, 'Produto Dentro', '3003201602371155984879.jpg', NULL, 'produto-dentro', 'SIM', 'CONFIRA NOSSOS PRODUTOS'),
(9, 'Orçamento', '3003201603571307163981.jpg', NULL, 'orcamento', 'SIM', 'MEU ORÇAMENTO'),
(10, 'Mobile Produtos', '3103201608071298272697.jpg', NULL, 'mobile-produtos', 'SIM', 'CONFIRA NOSSOS PRODUTOS'),
(11, 'Mobile Produtos Dentro', '3103201608191221160499.jpg', NULL, 'mobile-produtos-dentro', 'SIM', 'CONFIRA NOSSOS PRODUTOS'),
(12, 'Mobile Dicas', '3103201609381347148532.jpg', NULL, 'mobile-dicas', 'SIM', 'CONFIRA NOSSAS DICAS'),
(13, 'Mobile Dicas Dentro', '3103201611461389111739.jpg', NULL, 'mobile-dicas-dentro', 'SIM', 'NOSSAS DICAS'),
(14, 'Mobile Fale Conosco', '0104201612001216643720.jpg', NULL, 'mobile-fale-conosco', 'SIM', 'ENTRE EM CONTATO CONOSCO'),
(15, 'Mobile Trabalhe Conosco', '0104201612291318121734.jpg', NULL, 'mobile-trabalhe-conosco', 'SIM', 'VENHA TRABALHAR CONOSCO'),
(16, 'Mobile Portfólios', '0104201612431383520500.jpg', NULL, 'mobile-portfolios', 'SIM', 'CONFIRA NOSSO PORTFÓLIO'),
(17, 'Mobile Portfólios Dentro', '0104201612571266935561.jpg', NULL, 'mobile-portfolios-dentro', 'SIM', 'CONFIRA NOSSOS TRABALHOS'),
(18, 'Mobile Orçamentos', '0104201601131245918041.jpg', NULL, 'mobile-orcamentos', 'SIM', 'MEU ORÇAMENTO');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias_produtos`
--

CREATE TABLE `tb_categorias_produtos` (
  `idcategoriaproduto` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desCategoria_Dica_Modelcricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_categorias_produtos`
--

INSERT INTO `tb_categorias_produtos` (`idcategoriaproduto`, `titulo`, `desCategoria_Dica_Modelcricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(71, 'FOGUETES', NULL, '2903201612481254671914.png', 'SIM', NULL, 'foguetes', '', '', ''),
(72, 'FOGOS INDOOR', NULL, '2903201612491253688058.png', 'SIM', NULL, 'fogos-indoor', '', '', ''),
(74, 'GIRANDOLAS', NULL, '1507201610306550122569.png', 'SIM', NULL, 'girandolas', '', '', ''),
(75, 'TORTAS', NULL, '1507201610302652721491.png', 'SIM', NULL, 'tortas', '', '', ''),
(76, 'FUMAÇAS', NULL, '1507201610302916820028.png', 'SIM', NULL, 'fumacas', '', '', ''),
(77, 'INFANTO JUVENIL', NULL, '1507201610308128392025.png', 'SIM', NULL, 'infanto-juvenil', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_dicas`
--

CREATE TABLE `tb_comentarios_dicas` (
  `idcomentariodica` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_dica` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_produtos`
--

CREATE TABLE `tb_comentarios_produtos` (
  `idcomentarioproduto` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_configuracoes`
--

CREATE TABLE `tb_configuracoes` (
  `idconfiguracao` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `src_place` varchar(255) NOT NULL,
  `horario_1` varchar(255) DEFAULT NULL,
  `horario_2` varchar(255) DEFAULT NULL,
  `email_copia` varchar(255) DEFAULT NULL,
  `telefone3` varchar(255) DEFAULT NULL,
  `telefone4` varchar(255) DEFAULT NULL,
  `nome_tb_sitemaps` varchar(255) DEFAULT NULL,
  `google_plus` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_configuracoes`
--

INSERT INTO `tb_configuracoes` (`idconfiguracao`, `titulo`, `ativo`, `ordem`, `url_amigavel`, `endereco`, `telefone1`, `telefone2`, `email`, `src_place`, `horario_1`, `horario_2`, `email_copia`, `telefone3`, `telefone4`, `nome_tb_sitemaps`, `google_plus`) VALUES
(1, NULL, 'SIM', 0, '', 'EQNL 09/11, Bloco C, Loja 03  Taguatinga - Brasília -  DF', '(61) 3336-2400', '(62) 3922-2606', 'contato@milfogos.com.br', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15358.24211909388!2d-47.886154!3d-15.774371!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8d8f1a03bc3610ea!2sMil+Fogos+de+Artif%C3%ADcio+Asa+Norte!5e0!3m2!1spt-BR!2sbr!4v1468608844635', NULL, NULL, 'marciomas@gmail.com, junior@homewebbrasil.com.br, angela.homeweb@gmail.com', '(61) 8124-5605', '(62) 8150-1000', NULL, 'https://plus.google.com/104477564918871312937');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_depoimentos`
--

CREATE TABLE `tb_depoimentos` (
  `iddepoimento` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_depoimentos`
--

INSERT INTO `tb_depoimentos` (`iddepoimento`, `titulo`, `descricao`, `ativo`, `ordem`, `url_amigavel`, `imagem`) VALUES
(1, 'João Paulo', '<p>\r\n	Jo&atilde;o Paulo Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'joao-paulo', '1703201603181368911340..jpg'),
(2, 'Ana Júlia', '<p>\r\n	Ana J&uacute;lia&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'ana-julia', '0803201607301356333056..jpg'),
(3, 'Tatiana Alves', '<p>\r\n	Tatiana&nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'tatiana-alves', '0803201607301343163616.jpeg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_dicas`
--

CREATE TABLE `tb_dicas` (
  `iddica` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_dicas`
--

INSERT INTO `tb_dicas` (`iddica`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(42, 'Quais os procedimentos de seguranças a tomar?', '<p>\r\n	<strong>QUAIS OS PROCEDIMENTOS DE SEGURAN&Ccedil;A A TOMAR?</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- sempre leia e siga as instru&ccedil;&otilde;es na embalagem;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- sempre use fogos em locais abertos;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;-sempre solte fogos sob a supervis&atilde;o de adultos e de acordo com a sua idade;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- NUNCA tente reutilizar os fogos que tenham falhado;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- NUNCA atire fogos na dire&ccedil;&atilde;o de outras pessoas;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- NUNCA fa&ccedil;a experi&ecirc;ncias, modifique ou tente fazer seus pr&oacute;prios fogos;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- NUNCA utilize fogos ap&oacute;s ingerir BEBIDAS ALC&Oacute;OLICAS;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- NUNCA transporte estes artefatos nos bolsos, pois se eles se inflamarem, voc&ecirc; certamente ser&aacute; atingido;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- Se uma bombinha explodir nas m&atilde;os de uma crian&ccedil;a ou pr&oacute;ximo de seus olhos, poder&aacute; causar mutila&ccedil;&atilde;o ou cegueira;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- N&atilde;o soltar fogos em vias p&uacute;blicas;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- Certifique-se que o material seja de boa qualidade e que tenha informa&ccedil;&otilde;es sobre o fabricante, al&eacute;m das instru&ccedil;&otilde;es de uso;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- Recuse produtos vendidos em locais n&atilde;o autorizados (portas de garagem, ambulantes, armaz&eacute;ns etc.)</p>', '1507201609084540781051..jpg', 'SIM', NULL, 'quais-os-procedimentos-de-segurancas-a-tomar', '', '', '', NULL),
(48, 'Dicas para proteger seu amiguinho', '<p>\r\n	Determinadas &eacute;pocas do ano s&atilde;o previs&iacute;veis os fogos de artif&iacute;cio cortando os c&eacute;us. S&atilde;o lindos, mas s&atilde;o tamb&eacute;m respons&aacute;veis por muitos acidentes, n&atilde;o s&oacute; com pessoas mas tamb&eacute;m com os nossos animais. O per&iacute;odo junino, Copa do Mundo, finais de campeonato e... Reveillon!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Apresentamos algumas orienta&ccedil;&otilde;es, dicas que ajudar&atilde;o seu pet a assustar-se menos, j&aacute; que o barulho dos fogos e roj&otilde;es representa para eles verdadeiro p&acirc;nico, e desorientado ele corre sem destino.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Procure reduzir tudo isso garantindo condi&ccedil;&otilde;es m&iacute;nimas de seguran&ccedil;a, evitando ambientes conturbados e barulhentos (desde antes do espocar dos fogos), passe-lhe paz e tranq&uuml;ilidade, e a sensa&ccedil;&atilde;o de que tudo est&aacute; bem e sob controle.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>C&atilde;es:</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Acomodar os animais dentro de casa, em lugar onde possam se sentir em seguran&ccedil;a, com ilumina&ccedil;&atilde;o suave e se poss&iacute;vel um r&aacute;dio ligado com m&uacute;sica;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fechar portas e janelas para evitar fugas e suic&iacute;dios;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Dar alimentos leves, pois dist&uacute;rbios digestivos provocados pelo p&acirc;nico podem matar (tor&ccedil;&atilde;o de est&ocirc;mago, por exemplo);</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Cobrir gaiolas de p&aacute;ssaros e checar cercados de animais (cabras, galinhas etc.);</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Cobertores pesados estendidos nas janela abafam o som, assim como cobertores no ch&atilde;o ou um edredom sobre o animal;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	N&atilde;o deixar muitos c&atilde;es juntos. Excitados pelo barulho, podem brigar at&eacute; a morte. Tente deix&aacute;-los em quartos separados pois na hora dos fogos, no desespero, eles poder&atilde;o morder-se uns aos outros;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Um pouco antes da meia-noite leve seu animal para perto da tv ou de um aparelho de som e aumente aos poucos o volume para que ele se distraia e se acostume com um som alto. Assim n&atilde;o ficar&aacute; t&atilde;o assustado com o barulho forte e inesperado dos fogos;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Procurar um veterin&aacute;rio para sedar os animais no caso de n&atilde;o poder coloc&aacute;-los para dentro de casa. Animais acorrentados acabam se enforcando em fun&ccedil;&atilde;o do p&acirc;nico.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Alguns veterin&aacute;rios aconselham o uso de tamp&otilde;es de algod&atilde;o nos ouvidos que podem ser colocados minutos antes e tirados logo ap&oacute;s os fogos;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Calmantes naturais costumam apresentar resultado bastante eficiente em animais que historicamente apresentam muito estresse.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Gatos:</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Escolha um quarto da casa que tenha uma cama e um arm&aacute;rio, e prepare para ser o quarto dos gatos no reveillon. Abra um ou dois arm&aacute;rios e coloque cobertores para forrar e formar tocas confort&aacute;veis;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Desarrume a cama e coloque cobertores formando tocas; tocas embaixo da cama tamb&eacute;m s&atilde;o boas;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Feche toda a janela, passe a cortina e, se poss&iacute;vel, encoste um colch&atilde;o na janela para abafar o barulho;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&Aacute;gua, comida e caixinha de areia devem ficar distribu&iacute;dos estrategicamente pelo quarto, sempre encostados na parede, para evitar serem derrubados e suje tudo;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Tire qualquer coisa que possa ser derrubada, quebrada, derramada (lumin&aacute;rias, r&aacute;dio-rel&oacute;gio, extens&otilde;es...) ;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Feche os gatos neste quarto a partir dos primeiros roj&otilde;es e deixe-os l&aacute;. Deix&aacute;-los soltos aumenta o medo, a correria e o desespero, e eles acabam se enfiando em lugares muito escondidos e complicados, como por tr&aacute;s da m&aacute;quina de lavar ou da geladeira;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para quem mora em casa, com gatos que tem acesso &agrave; rua, recolha os gatos antes do p&ocirc;r-do-sol e feche-os da mesma maneira. Na rua &eacute; mais perigoso, pois quando se assustarem, podem se perder. Al&eacute;m disso, podem ser alvo de maus-tratos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	E com todos esses cuidados, certamente voc&ecirc; e ele passar&atilde;o uma alegre noite!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Equipe Eu Amo Animais aproveita para desejar a todos um Feliz 2011!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	(artigo compilado de diversas fontes na WEB)</p>', '1507201605597713984267..jpg', 'SIM', NULL, 'dicas-para-proteger-seu-amiguinho', '', '', '', NULL),
(43, 'Quais os primeiros socorros?', '<p>\r\n	<strong>QUAIS OS PRIMEIROS SOCORROS?</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- Lavar a &aacute;rea atingida com &aacute;gua corrente, de prefer&ecirc;ncia &aacute;gua gelada.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- Enquanto n&atilde;o houver atendimento no hospital, cobrir a queimadura com um pano limpo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- Nas queimaduras em pequenas &aacute;reas pode-se passar vaselina esterilizada.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- Nunca fure as bolhas! Elas servem para proteger a &aacute;rea queimada.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- N&atilde;o retire roupas grudadas, fragmentos de objetos ou graxas das les&otilde;es.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- N&atilde;o use pomadas sem ordem m&eacute;dica, nem toque as les&otilde;es com as m&atilde;os.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>- Procure Socorro M&eacute;dico!</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- Se houver sangramento, fa&ccedil;a um curativo com gaze ou um pano bem limpo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- Os pacientes com grandes &aacute;reas queimadas devem ser mantidos bem hidratados. Enquanto n&atilde;o chegar ao hospital, procure fazer o acidentado beber bastante &aacute;gua, pois a perda de &aacute;gua nas &aacute;reas queimadas &eacute; muito grande.ATEN&Ccedil;&Atilde;O: n&atilde;o tente dar &aacute;gua se a pessoa estiver inconsciente!</p>', '1507201609093922825683..jpg', 'SIM', NULL, 'quais-os-primeiros-socorros', '', '', '', NULL),
(47, 'Dicas para compra de fogos de artifícios', '<p>\r\n	Apesar de agrad&aacute;veis de ser apreciados, os fogos de artif&iacute;cios exigem cuidados. Confira a seguir algumas dicas de compra e manuseio dos artefatos:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Sempre compre os fogos em locais autorizados pelo Corpo de Bombeiros, evitando camel&ocirc;s e empresas de &ldquo;fundo de quintal&rdquo;. Antes de efetivar a compra, verifique as informa&ccedil;&otilde;es obrigat&oacute;rias na embalagem: instru&ccedil;&otilde;es de uso, nome correto do fogo de artif&iacute;cio (foguete, roj&atilde;o etc.), a data de fabrica&ccedil;&atilde;o e validade, peso bruto e peso l&iacute;quido, n&uacute;mero de registro no Ex&eacute;rcito, respons&aacute;vel t&eacute;cnico e seu registro profissional (em geral, engenheiro qu&iacute;mico), proced&ecirc;ncia (fabricante e comerciante e respectivos CNPJ e endere&ccedil;o), bem como a classe do produto. &Eacute; importante que o consumidor n&atilde;o adquira fogos sem embalagem.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Siga as instru&ccedil;&otilde;es de uso do fabricante contidas na embalagem. N&atilde;o solte fogos sobre o efeito de bebidas alco&oacute;licas e nem pr&oacute;ximo a hospitais, fia&ccedil;&otilde;es el&eacute;tricas ou &aacute;rvores, procure realizar a queima dos fogos em locais abertos e sem vegeta&ccedil;&atilde;o nas proximidades. Nunca aponte os fogos para pessoas, e, antes de solt&aacute;-los, verifique se n&atilde;o h&aacute; materiais combust&iacute;veis nas proximidades. Outra dica importante &eacute; n&atilde;o tentar reascender fogos que falharem.</p>\r\n<p>\r\n	Em caso de queimaduras leves, procure aliviar a dor mergulhando a parte afetada em &aacute;gua pot&aacute;vel. Nunca aplique creme dental, sab&atilde;o, borra de caf&eacute;, manteiga ou azeite. Em casos mais graves, como queimaduras no rosto, de 2&ordm; e de 3&ordm; graus, ou em muitas partes do corpo, acione o Corpo de Bombeiros.</p>', '1507201605495583446696..jpg', 'SIM', NULL, 'dicas-para-compra-de-fogos-de-artificios', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_empresa`
--

CREATE TABLE `tb_empresa` (
  `idempresa` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `colaboradores_especializados` int(11) DEFAULT NULL,
  `trabalhos_entregues` int(11) DEFAULT NULL,
  `trabalhos_entregues_este_mes` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_empresa`
--

INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `descricao`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`, `colaboradores_especializados`, `trabalhos_entregues`, `trabalhos_entregues_este_mes`) VALUES
(3, 'Empresa - Descrição', '<p>\r\n	Buscando oferecer o que h&aacute; de mais moderno e surpreendente em shows pirot&eacute;cnicos indoor e outdoor, a Mil Fogos realiza eventos e espet&aacute;culos que utilizem efeitos especiais, n&atilde;o apenas em Bras&iacute;lia mas em todas as regi&otilde;es do Brasil.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Atuando desde 1993 a empresa tem como um de seus maiores objetivos transformar cada apresenta&ccedil;&atilde;o em um momento &uacute;nico para os seus clientes e espectadores, ao oferecer um servi&ccedil;o de alta qualidade que une sempre beleza e seguran&ccedil;a por um pre&ccedil;o acess&iacute;vel.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	No profissionalismo presente em todos os eventos dos quais j&aacute; fez parte &ndash; desde casamentos, anivers&aacute;rios, formaturas e r&eacute;veillons &agrave; abertura de shows, lan&ccedil;amento de produtos, comercias, festas de rodeio, entre outros &ndash; fica clara a inten&ccedil;&atilde;o da Mil Fogos em se tornar refer&ecirc;ncia em planejamento, projetos e execu&ccedil;&atilde;o de shows pirot&eacute;cnicos e efeitos especiais in-door no pa&iacute;s.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Equipe de designer&#39;s altamente qualificada para elabora&ccedil;&atilde;o de projeto e execu&ccedil;&atilde;o com profissionalismo e seguran&ccedil;a.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Equipamentos de ultima gera&ccedil;&atilde;o. Disparos em ambiente fechado com equipamentos sem fio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Estrutura para execu&ccedil;&atilde;o de 25 eventos simult&acirc;neos.</strong></p>\r\n<div>\r\n	&nbsp;</div>', 'SIM', 0, '', '', '', 'empresa--descricao', NULL, NULL, NULL),
(4, 'Index - CONHEÇA MAIS A MIL FOGOS', '<p>\r\n	Buscando oferecer o que h&aacute; de mais moderno e surpreendente em shows pirot&eacute;cnicos indoor e outdoor, a Mil Fogos realiza eventos e espet&aacute;culos que utilizem efeitos especiais, n&atilde;o apenas em Bras&iacute;lia mas em todas as regi&otilde;es do Brasil.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Atuando desde 1993 a empresa tem como um de seus maiores objetivos transformar cada apresenta&ccedil;&atilde;o em um momento &uacute;nico para os seus clientes e espectadores, ao oferecer um servi&ccedil;o de alta qualidade que une sempre beleza e seguran&ccedil;a por um pre&ccedil;o acess&iacute;vel.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	No profissionalismo presente em todos os eventos dos quais j&aacute; fez parte &ndash; desde casamentos, anivers&aacute;rios, formaturas e r&eacute;veillons &agrave; abertura de shows, lan&ccedil;amento de produtos, comercias, festas de rodeio, entre outros &ndash; fica clara a inten&ccedil;&atilde;o da Mil Fogos em se tornar refer&ecirc;ncia em planejamento, projetos e execu&ccedil;&atilde;o de shows pirot&eacute;cnicos e efeitos especiais in-door no pa&iacute;s.</p>\r\n<div>\r\n	&nbsp;</div>', 'SIM', 0, '', '', '', 'index--conheca-mais-a-mil-fogos', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_especificacoes`
--

CREATE TABLE `tb_especificacoes` (
  `idespecificacao` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `title_google` longtext,
  `keywords_google` longtext,
  `description_google` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_especificacoes`
--

INSERT INTO `tb_especificacoes` (`idespecificacao`, `titulo`, `imagem`, `url_amigavel`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`) VALUES
(1, 'Porta Fixa Por Dentro do Vão', '0803201611341367322959..jpg', 'porta-fixa-por-dentro-do-vao', 'SIM', NULL, NULL, NULL, NULL),
(2, 'Porta Fixa Por Trás do Vão', '0803201611341157385324..jpg', 'porta-fixa-por-tras-do-vao', 'SIM', NULL, NULL, NULL, NULL),
(3, 'Formas de Fixação das Guias', '0803201611341304077829..jpg', 'formas-de-fixacao-das-guias', 'SIM', NULL, NULL, NULL, NULL),
(4, 'Vista Lateral do Rolo da Porta', '0803201611351168393570..jpg', 'vista-lateral-do-rolo-da-porta', 'SIM', NULL, NULL, NULL, NULL),
(5, 'Portinhola Lateral', '0803201611351116878064..jpg', 'portinhola-lateral', 'SIM', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_portifolios`
--

CREATE TABLE `tb_galerias_portifolios` (
  `idgaleriaportifolio` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_portifolio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_portifolios`
--

INSERT INTO `tb_galerias_portifolios` (`idgaleriaportifolio`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_portifolio`) VALUES
(6, '1503201609571185453289.jpg', 'SIM', NULL, NULL, 2),
(7, '1503201609571385251299.jpg', 'SIM', NULL, NULL, 2),
(8, '1503201609571398241846.jpg', 'SIM', NULL, NULL, 2),
(9, '1503201609571372148996.jpg', 'SIM', NULL, NULL, 2),
(10, '1503201609571203846190.jpg', 'SIM', NULL, NULL, 2),
(11, '1503201609571209439705.jpg', 'SIM', NULL, NULL, 3),
(12, '1503201609571247186947.jpg', 'SIM', NULL, NULL, 3),
(13, '1503201609571183328677.jpg', 'SIM', NULL, NULL, 3),
(14, '1503201609571245061526.jpg', 'SIM', NULL, NULL, 3),
(15, '1503201609571132779946.jpg', 'SIM', NULL, NULL, 3),
(16, '1503201609571208483876.jpg', 'SIM', NULL, NULL, 4),
(17, '1503201609571274489300.jpg', 'SIM', NULL, NULL, 4),
(18, '1503201609571406945852.jpg', 'SIM', NULL, NULL, 4),
(19, '1503201609571220302542.jpg', 'SIM', NULL, NULL, 4),
(20, '1503201609571348685064.jpg', 'SIM', NULL, NULL, 4),
(21, '1503201609571281798209.jpg', 'SIM', NULL, NULL, 5),
(22, '1503201609571119695620.jpg', 'SIM', NULL, NULL, 5),
(23, '1503201609571342930547.jpg', 'SIM', NULL, NULL, 5),
(24, '1503201609571333131668.jpg', 'SIM', NULL, NULL, 5),
(25, '1503201609571184904665.jpg', 'SIM', NULL, NULL, 5),
(26, '1603201602001119086460.jpg', 'SIM', NULL, NULL, 6),
(27, '1603201602001399143623.jpg', 'SIM', NULL, NULL, 6),
(28, '1603201602001370562965.jpg', 'SIM', NULL, NULL, 6),
(29, '1603201602001360716700.jpg', 'SIM', NULL, NULL, 6),
(30, '1603201602001161033394.jpg', 'SIM', NULL, NULL, 6),
(31, '1603201602001294477762.jpg', 'SIM', NULL, NULL, 7),
(32, '1603201602001391245593.jpg', 'SIM', NULL, NULL, 7),
(33, '1603201602001270831865.jpg', 'SIM', NULL, NULL, 7),
(34, '1603201602001379540967.jpg', 'SIM', NULL, NULL, 7),
(35, '1603201602001260348087.jpg', 'SIM', NULL, NULL, 7),
(36, '1207201608431870830878.jpg', 'SIM', NULL, NULL, 8),
(37, '1207201608536031081212.jpg', 'SIM', NULL, NULL, 9),
(38, '1507201609244925004885.jpg', 'SIM', NULL, NULL, 11),
(39, '1507201609256857368999.jpg', 'SIM', NULL, NULL, 11),
(40, '1507201609256853751457.jpg', 'SIM', NULL, NULL, 11),
(41, '1507201609259990444059.jpg', 'SIM', NULL, NULL, 11),
(42, '1507201609256707053573.jpg', 'SIM', NULL, NULL, 11),
(43, '1507201609252273439938.jpg', 'SIM', NULL, NULL, 11);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_produtos`
--

CREATE TABLE `tb_galerias_produtos` (
  `id_galeriaproduto` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_produtos`
--

INSERT INTO `tb_galerias_produtos` (`id_galeriaproduto`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`) VALUES
(1, '0703201609071140150439.jpg', 'SIM', NULL, NULL, 1),
(2, '0703201609071136889835.jpg', 'SIM', NULL, NULL, 1),
(3, '0703201609071310022432.jpg', 'SIM', NULL, NULL, 1),
(4, '0703201609071332883688.jpg', 'SIM', NULL, NULL, 1),
(5, '0703201609071325901563.jpg', 'SIM', NULL, NULL, 1),
(6, '0703201609091113210084.jpg', 'SIM', NULL, NULL, 7),
(7, '0703201609091231870487.jpg', 'SIM', NULL, NULL, 7),
(8, '0703201609091392892564.jpg', 'SIM', NULL, NULL, 7),
(9, '0703201609091328054436.jpg', 'SIM', NULL, NULL, 7),
(10, '0703201609091306414589.jpg', 'SIM', NULL, NULL, 7),
(11, '0703201609091372108288.jpg', 'SIM', NULL, NULL, 7),
(12, '0703201609181260343119.jpg', 'SIM', NULL, NULL, 7),
(13, '0703201609181249140112.jpg', 'SIM', NULL, NULL, 7),
(14, '0703201609181133604738.jpg', 'SIM', NULL, NULL, 7),
(15, '0703201609181353990372.jpg', 'SIM', NULL, NULL, 7),
(17, '0703201609181258088156.jpg', 'SIM', NULL, NULL, 7),
(18, '0703201609181216159805.jpg', 'SIM', NULL, NULL, 7),
(19, '0703201609181324792290.jpg', 'SIM', NULL, NULL, 7),
(21, '0703201609191393295642.jpg', 'SIM', NULL, NULL, 2),
(22, '0703201609191348444471.jpg', 'SIM', NULL, NULL, 2),
(23, '0703201609191315103564.jpg', 'SIM', NULL, NULL, 2),
(24, '0703201609191245110268.jpg', 'SIM', NULL, NULL, 2),
(25, '0703201609191340441345.jpg', 'SIM', NULL, NULL, 2),
(26, '0703201609191329932562.jpg', 'SIM', NULL, NULL, 2),
(28, '1503201609561319044858.jpg', 'SIM', NULL, NULL, 3),
(29, '1503201609561123662081.jpg', 'SIM', NULL, NULL, 3),
(30, '1503201609561206848839.jpg', 'SIM', NULL, NULL, 3),
(31, '1503201609561331659456.jpg', 'SIM', NULL, NULL, 3),
(32, '1503201609561244715085.jpg', 'SIM', NULL, NULL, 3),
(33, '1503201609561286452532.jpg', 'SIM', NULL, NULL, 4),
(34, '1503201609561167966515.jpg', 'SIM', NULL, NULL, 4),
(35, '1503201609561317434222.jpg', 'SIM', NULL, NULL, 4),
(36, '1503201609561269009510.jpg', 'SIM', NULL, NULL, 4),
(37, '1503201609561274119030.jpg', 'SIM', NULL, NULL, 4),
(38, '1503201609561349582165.jpg', 'SIM', NULL, NULL, 5),
(39, '1503201609561326914421.jpg', 'SIM', NULL, NULL, 5),
(40, '1503201609561319011268.jpg', 'SIM', NULL, NULL, 5),
(41, '1503201609561360905328.jpg', 'SIM', NULL, NULL, 5),
(42, '1503201609561199681654.jpg', 'SIM', NULL, NULL, 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logins`
--

CREATE TABLE `tb_logins` (
  `idlogin` int(11) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `acesso_tags` varchar(3) DEFAULT 'NAO',
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logins`
--

INSERT INTO `tb_logins` (`idlogin`, `titulo`, `senha`, `ativo`, `id_grupologin`, `email`, `acesso_tags`, `url_amigavel`) VALUES
(1, 'Homeweb', 'e10adc3949ba59abbe56e057f20f883e', 'SIM', 0, 'atendimento.sites@homewebbrasil.com.br', 'SIM', NULL),
(2, 'Marcio André', '202cb962ac59075b964b07152d234b70', 'SIM', 0, 'marciomas@gmail.com', 'NAO', 'marcio-andre');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logs_logins`
--

CREATE TABLE `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logs_logins`
--

INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(1, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-11', '00:50:03', 1),
(2, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-11', '00:50:11', 1),
(3, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:18:12', 1),
(4, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:18:38', 1),
(5, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:19:57', 1),
(6, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:15:44', 1),
(7, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:16:58', 1),
(8, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:20:30', 1),
(9, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:21:15', 1),
(10, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:22:14', 1),
(11, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:22:27', 1),
(12, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:23:12', 1),
(13, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:23:34', 1),
(14, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:34:29', 1),
(15, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '20:42:14', 1),
(16, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '20:45:13', 1),
(17, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '21:06:59', 1),
(18, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '21:07:22', 1),
(19, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:21:44', 1),
(20, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:22:01', 1),
(21, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'1\'', '2016-03-02', '22:23:41', 1),
(22, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:24:30', 1),
(23, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:24:52', 1),
(24, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:24:56', 1),
(25, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:25:06', 1),
(26, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'1\'', '2016-03-02', '22:27:22', 1),
(27, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:27:25', 1),
(28, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:27:28', 1),
(29, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:27:31', 1),
(30, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'1\'', '2016-03-02', '22:28:19', 1),
(31, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:28:22', 1),
(32, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:28:37', 1),
(33, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:28:39', 1),
(34, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:28:42', 1),
(35, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:29:26', 1),
(36, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:29:31', 1),
(37, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:29:57', 1),
(38, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:30:01', 1),
(39, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:32:07', 1),
(40, 'DESATIVOU O LOGIN 3', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'3\'', '2016-03-02', '22:32:13', 1),
(41, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'3\'', '2016-03-02', '22:32:13', 1),
(42, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:32:41', 1),
(43, 'DESATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'4\'', '2016-03-02', '22:32:46', 1),
(44, 'ATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'4\'', '2016-03-02', '22:32:49', 1),
(45, 'DESATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'4\'', '2016-03-02', '22:32:51', 1),
(46, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'4\'', '2016-03-02', '22:32:54', 1),
(47, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:33:10', 1),
(48, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'5\'', '2016-03-02', '22:34:16', 1),
(49, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:34:35', 1),
(50, 'DESATIVOU O LOGIN 6', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'6\'', '2016-03-02', '22:38:39', 1),
(51, 'ATIVOU O LOGIN 6', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'6\'', '2016-03-02', '22:38:44', 1),
(52, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'6\'', '2016-03-02', '22:38:47', 1),
(53, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:41:49', 1),
(54, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:40:19', 0),
(55, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:41:03', 0),
(56, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:41:35', 0),
(57, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:01', 0),
(58, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:21', 0),
(59, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:34', 0),
(60, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:50', 0),
(61, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:43:06', 0),
(62, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:56:12', 0),
(63, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:57:39', 0),
(64, 'DESATIVOU O LOGIN 43', 'UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'43\'', '2016-03-07', '21:58:16', 0),
(65, 'ATIVOU O LOGIN 43', 'UPDATE tb_dicas SET ativo = \'SIM\' WHERE iddica = \'43\'', '2016-03-07', '21:58:18', 0),
(66, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:59:03', 0),
(67, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:59:09', 0),
(68, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:34', 0),
(69, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:44', 0),
(70, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:56', 0),
(71, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:09:35', 0),
(72, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:10:27', 0),
(73, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:12:58', 0),
(74, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:14:20', 0),
(75, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:15:08', 0),
(76, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:27:15', 0),
(77, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:29:53', 0),
(78, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:30:18', 0),
(79, 'DESATIVOU O LOGIN 44', 'UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'44\'', '2016-03-08', '00:43:43', 0),
(80, 'ATIVOU O LOGIN 44', 'UPDATE tb_dicas SET ativo = \'SIM\' WHERE iddica = \'44\'', '2016-03-08', '00:43:48', 0),
(81, 'DESATIVOU O LOGIN 46', 'UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'46\'', '2016-03-08', '00:43:53', 0),
(82, 'DESATIVOU O LOGIN 45', 'UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'45\'', '2016-03-08', '00:43:56', 0),
(83, 'ATIVOU O LOGIN 46', 'UPDATE tb_dicas SET ativo = \'SIM\' WHERE iddica = \'46\'', '2016-03-08', '00:43:59', 0),
(84, 'ATIVOU O LOGIN 45', 'UPDATE tb_dicas SET ativo = \'SIM\' WHERE iddica = \'45\'', '2016-03-08', '00:44:02', 0),
(85, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:32', 0),
(86, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:43', 0),
(87, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:56', 0),
(88, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:55:31', 0),
(89, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:56:16', 0),
(90, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '20:50:57', 0),
(91, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:12', 0),
(92, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:42', 0),
(93, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:58', 0),
(94, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:20:14', 0),
(95, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:20:44', 0),
(96, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '22:22:06', 0),
(97, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '22:22:55', 0),
(98, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:20', 0),
(99, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:35', 0),
(100, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:56', 0),
(101, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:35:17', 0),
(102, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:35:39', 0),
(103, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-09', '13:51:08', 0),
(104, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:47:37', 3),
(105, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:48:04', 3),
(106, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:48:53', 3),
(107, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:49:40', 3),
(108, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: contato@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'3\'', '2016-03-11', '11:49:47', 3),
(109, 'DESATIVOU O LOGIN 2', 'UPDATE tb_logins SET ativo = \'NAO\' WHERE idlogin = \'2\'', '2016-03-11', '11:49:51', 3),
(110, 'ATIVOU O LOGIN 2', 'UPDATE tb_logins SET ativo = \'SIM\' WHERE idlogin = \'2\'', '2016-03-11', '11:49:53', 3),
(111, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:51:20', 3),
(112, 'CADASTRO DO LOGIN ', '', '2016-03-11', '12:35:06', 3),
(113, 'CADASTRO DO LOGIN ', '', '2016-03-11', '12:36:19', 3),
(114, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: contato@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'4\'', '2016-03-11', '12:36:27', 3),
(115, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: contato1@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'5\'', '2016-03-11', '12:36:30', 3),
(116, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: contato2@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'6\'', '2016-03-11', '12:36:32', 3),
(117, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = \'\', email = \'2\', id_grupologin = \'2\' WHERE idlogin = \'2\'', '2016-03-11', '12:37:48', 3),
(118, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = \'\', email = \'2\', id_grupologin = \'\' WHERE idlogin = \'2\'', '2016-03-11', '12:38:15', 3),
(119, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = \'\', email = \'2\' WHERE idlogin = \'2\'', '2016-03-11', '12:38:42', 3),
(120, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = \'2\', email = \'2\' WHERE idlogin = \'2\'', '2016-03-11', '12:39:26', 3),
(121, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:22:49', 3),
(122, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:23:39', 3),
(123, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:24:16', 3),
(124, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:25:10', 3),
(125, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:25:22', 3),
(126, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:42:02', 3),
(127, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:42:36', 3),
(128, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:46:24', 3),
(129, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:47:24', 3),
(130, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:57:19', 3),
(131, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '14:32:53', 3),
(132, 'CADASTRO DO CLIENTE ', '', '2016-03-14', '21:25:38', 0),
(133, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-14', '21:41:54', 0),
(134, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:45:22', 0),
(135, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:45:53', 0),
(136, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:46:22', 0),
(137, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:49:15', 0),
(138, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:49:46', 0),
(139, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = \'4\'', '2016-03-15', '21:50:04', 0),
(140, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = \'5\'', '2016-03-15', '21:50:07', 0),
(141, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:50:37', 0),
(142, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:51:02', 0),
(143, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-15', '22:40:48', 0),
(144, 'CADASTRO DO CLIENTE ', '', '2016-03-16', '14:00:02', 1),
(145, 'CADASTRO DO CLIENTE ', '', '2016-03-16', '14:00:28', 1),
(146, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-17', '15:18:52', 1),
(147, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-21', '14:35:37', 1),
(148, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-21', '14:37:17', 1),
(149, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '10:24:37', 1),
(150, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:07:25', 1),
(151, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:28:43', 1),
(152, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:28:59', 1),
(153, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:29:32', 1),
(154, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:29:40', 1),
(155, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '20:43:11', 1),
(156, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '21:54:11', 1),
(157, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '22:07:44', 1),
(158, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '22:54:16', 1),
(159, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:01:55', 1),
(160, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:03:06', 1),
(161, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_lojas WHERE idloja = \'1\'', '2016-03-28', '23:04:52', 1),
(162, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:05:18', 1),
(163, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:05:40', 1),
(164, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:12:34', 1),
(165, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:15:26', 1),
(166, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:26:58', 1),
(167, 'EXCLUSÃO DO LOGIN 70, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'70\'', '2016-03-29', '00:36:19', 1),
(168, 'EXCLUSÃO DO LOGIN 73, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'73\'', '2016-03-29', '00:36:30', 1),
(169, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:45:43', 1),
(170, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:48:41', 1),
(171, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:49:15', 1),
(172, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:35:27', 1),
(173, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:35:49', 1),
(174, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:36:09', 1),
(175, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:36:22', 1),
(176, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:36:51', 1),
(177, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:37:26', 1),
(178, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:38:26', 1),
(179, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '02:32:40', 1),
(180, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '02:37:17', 1),
(181, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '03:57:35', 1),
(182, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:32:44', 1),
(183, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:33:28', 1),
(184, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:33:57', 1),
(185, 'CADASTRO DO CLIENTE ', '', '2016-03-31', '15:18:07', 1),
(186, 'CADASTRO DO CLIENTE ', '', '2016-03-31', '15:21:45', 1),
(187, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '15:21:55', 1),
(188, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '20:07:11', 1),
(189, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '20:19:04', 1),
(190, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '21:38:40', 1),
(191, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '23:46:08', 1),
(192, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:00:34', 1),
(193, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:29:59', 1),
(194, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:43:01', 1),
(195, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:57:06', 1),
(196, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:03:21', 1),
(197, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:04:07', 1),
(198, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:04:52', 1),
(199, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:13:23', 1),
(200, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-11', '18:54:02', 1),
(201, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-11', '19:00:14', 1),
(202, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-11', '19:03:30', 1),
(203, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-11', '19:04:14', 1),
(204, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-11', '19:05:10', 1),
(205, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-11', '19:05:41', 1),
(206, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '20:23:57', 1),
(207, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '20:40:18', 1),
(208, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '20:42:17', 1),
(209, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_portfolios WHERE idportfolio     = \'1\'', '2016-07-12', '20:42:46', 1),
(210, 'CADASTRO DO CLIENTE ', '', '2016-07-12', '20:43:18', 1),
(211, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_portfolios WHERE idportfolio     = \'2\'', '2016-07-12', '20:44:15', 1),
(212, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_portfolios WHERE idportfolio     = \'3\'', '2016-07-12', '20:44:17', 1),
(213, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_portfolios WHERE idportfolio     = \'4\'', '2016-07-12', '20:44:20', 1),
(214, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_portfolios WHERE idportfolio     = \'5\'', '2016-07-12', '20:44:22', 1),
(215, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_portfolios WHERE idportfolio     = \'6\'', '2016-07-12', '20:44:24', 1),
(216, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_portfolios WHERE idportfolio     = \'7\'', '2016-07-12', '20:44:26', 1),
(217, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_portfolios WHERE idportfolio     = \'8\'', '2016-07-12', '20:45:36', 1),
(218, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '20:50:17', 1),
(219, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '20:51:11', 1),
(220, 'CADASTRO DO CLIENTE ', '', '2016-07-12', '20:53:14', 1),
(221, 'CADASTRO DO CLIENTE ', '', '2016-07-12', '21:02:23', 1),
(222, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '21:06:39', 1),
(223, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '21:06:56', 1),
(224, 'CADASTRO DO CLIENTE ', '', '2016-07-12', '21:07:22', 1),
(225, 'CADASTRO DO CLIENTE ', '', '2016-07-12', '21:09:10', 1),
(226, 'CADASTRO DO CLIENTE ', '', '2016-07-12', '21:09:36', 1),
(227, 'CADASTRO DO CLIENTE ', '', '2016-07-12', '21:10:01', 1),
(228, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '21:14:37', 1),
(229, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '21:16:12', 1),
(230, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '21:17:40', 1),
(231, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '21:21:47', 1),
(232, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '21:24:24', 1),
(233, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '14:07:15', 1),
(234, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '09:06:16', 1),
(235, 'EXCLUSÃO DO LOGIN 44, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'44\'', '2016-07-15', '09:06:22', 1),
(236, 'EXCLUSÃO DO LOGIN 45, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'45\'', '2016-07-15', '09:06:25', 1),
(237, 'EXCLUSÃO DO LOGIN 46, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'46\'', '2016-07-15', '09:06:27', 1),
(238, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '09:07:23', 1),
(239, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '09:08:14', 1),
(240, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '09:09:37', 1),
(241, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '09:11:27', 1),
(242, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '09:13:02', 1),
(243, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '09:14:32', 1),
(244, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '09:19:52', 1),
(245, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '09:24:47', 1),
(246, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '09:28:53', 1),
(247, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '09:29:52', 1),
(248, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '09:32:29', 1),
(249, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '09:33:26', 1),
(250, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '09:33:42', 1),
(251, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '09:33:57', 1),
(252, 'DESATIVOU O LOGIN 13', 'UPDATE tb_portfolios SET ativo = \'NAO\' WHERE idportfolio     = \'13\'', '2016-07-15', '09:34:54', 1),
(253, 'ATIVOU O LOGIN 13', 'UPDATE tb_portfolios SET ativo = \'SIM\' WHERE idportfolio     = \'13\'', '2016-07-15', '09:36:40', 1),
(254, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '09:41:19', 1),
(255, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '09:43:49', 1),
(256, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '09:44:50', 1),
(257, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '09:45:17', 1),
(258, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '09:47:54', 1),
(259, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '09:48:17', 1),
(260, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '09:51:13', 1),
(261, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '09:53:54', 1),
(262, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '09:57:43', 1),
(263, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '09:58:03', 1),
(264, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '10:00:50', 1),
(265, 'DESATIVOU O LOGIN 11', 'UPDATE tb_portfolios SET ativo = \'NAO\' WHERE idportfolio     = \'11\'', '2016-07-15', '10:05:20', 1),
(266, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '10:07:30', 1),
(267, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '10:09:32', 1),
(268, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '10:11:15', 1),
(269, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '10:12:09', 1),
(270, 'DESATIVOU O LOGIN 8', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'8\'', '2016-07-15', '10:12:16', 1),
(271, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '10:14:18', 1),
(272, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '10:15:45', 1),
(273, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '10:15:59', 1),
(274, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '10:17:52', 1),
(275, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '10:20:17', 1),
(276, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '10:22:46', 1),
(277, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '10:25:04', 1),
(278, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '10:25:39', 1),
(279, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '10:26:12', 1),
(280, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '10:27:25', 1),
(281, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '10:30:00', 1),
(282, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '10:30:09', 1),
(283, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '10:30:18', 1),
(284, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '10:30:26', 1),
(285, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '10:31:31', 1),
(286, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '10:33:25', 1),
(287, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '10:35:32', 1),
(288, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '10:36:11', 1),
(289, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '10:36:39', 1),
(290, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '10:37:31', 1),
(291, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '10:38:42', 1),
(292, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '10:42:37', 1),
(293, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '11:06:06', 1),
(294, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '11:07:47', 1),
(295, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '11:08:27', 1),
(296, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '11:09:31', 1),
(297, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '11:11:25', 1),
(298, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '11:14:17', 1),
(299, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '11:15:10', 1),
(300, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '11:16:24', 1),
(301, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '11:18:58', 1),
(302, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '11:20:33', 1),
(303, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '11:21:39', 1),
(304, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '11:22:55', 1),
(305, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '11:23:45', 1),
(306, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '11:24:26', 1),
(307, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '11:33:06', 1),
(308, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '11:35:28', 1),
(309, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '11:38:57', 1),
(310, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '11:53:29', 1),
(311, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '11:58:07', 1),
(312, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '11:58:28', 1),
(313, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '11:58:57', 1),
(314, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '12:03:08', 1),
(315, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '12:06:04', 1),
(316, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '12:08:30', 1),
(317, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '12:11:11', 1),
(318, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '12:12:01', 1),
(319, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '12:13:02', 1),
(320, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '12:23:07', 1),
(321, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '12:33:27', 1),
(322, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '12:41:57', 1),
(323, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '12:47:59', 1),
(324, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '12:51:47', 1),
(325, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '13:02:26', 1),
(326, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '13:09:05', 1),
(327, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '13:13:14', 1),
(328, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '13:16:23', 1),
(329, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '13:18:30', 1),
(330, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '13:20:48', 1),
(331, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '13:21:54', 1),
(332, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '13:34:52', 1),
(333, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '13:36:04', 1),
(334, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '13:39:11', 1),
(335, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_lojas WHERE idloja = \'3\'', '2016-07-15', '13:39:20', 1),
(336, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '14:04:38', 1),
(337, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_lojas WHERE idloja = \'4\'', '2016-07-15', '14:04:50', 1),
(338, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '14:06:02', 1),
(339, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '14:06:17', 1),
(340, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '14:12:39', 1),
(341, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '14:13:24', 1),
(342, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '14:14:57', 1),
(343, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '14:16:56', 1),
(344, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '14:18:22', 1),
(345, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '14:20:07', 1),
(346, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '14:21:36', 1),
(347, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '14:23:55', 1),
(348, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '14:26:08', 1),
(349, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '14:26:50', 1),
(350, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '14:28:59', 1),
(351, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '14:30:40', 1),
(352, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '14:32:12', 1),
(353, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '14:33:22', 1),
(354, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '14:35:00', 1),
(355, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '14:36:56', 1),
(356, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '14:38:19', 1),
(357, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '14:40:06', 1),
(358, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '14:42:26', 1),
(359, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '15:16:08', 1),
(360, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '15:17:15', 1),
(361, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '15:33:37', 1),
(362, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '15:45:11', 1),
(363, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '15:50:14', 1),
(364, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '15:54:25', 1),
(365, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '16:41:01', 1),
(366, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '16:43:03', 1),
(367, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '16:45:11', 1),
(368, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '16:52:04', 1),
(369, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '16:57:30', 1),
(370, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '16:57:56', 1),
(371, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '16:58:26', 1),
(372, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '17:00:54', 1),
(373, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '17:03:22', 1),
(374, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '17:05:06', 1),
(375, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '17:46:09', 1),
(376, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '17:46:43', 1),
(377, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '17:49:40', 1),
(378, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '17:54:45', 1),
(379, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '17:57:34', 1),
(380, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '17:59:32', 1),
(381, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:10:51', 1),
(382, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:11:37', 1),
(383, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:12:08', 1),
(384, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:12:24', 1),
(385, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:12:42', 1),
(386, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:12:53', 1),
(387, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:13:08', 1),
(388, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:13:31', 1),
(389, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:13:43', 1),
(390, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:14:14', 1),
(391, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:14:27', 1),
(392, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:14:44', 1),
(393, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:14:58', 1),
(394, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:15:10', 1),
(395, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:15:27', 1),
(396, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:15:39', 1),
(397, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:16:15', 1),
(398, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:16:39', 1),
(399, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:16:50', 1),
(400, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:17:08', 1),
(401, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:17:34', 1),
(402, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:17:46', 1),
(403, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:18:04', 1),
(404, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:18:23', 1),
(405, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:18:44', 1),
(406, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:25:46', 1),
(407, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:26:47', 1),
(408, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:27:21', 1),
(409, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:27:45', 1),
(410, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:28:17', 1),
(411, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:28:52', 1),
(412, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:29:16', 1),
(413, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:29:39', 1),
(414, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:29:59', 1),
(415, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:30:25', 1),
(416, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:30:50', 1),
(417, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:31:14', 1),
(418, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '18:32:44', 1),
(419, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:41:23', 1),
(420, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:42:58', 1),
(421, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:44:22', 1),
(422, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:46:46', 1),
(423, 'CADASTRO DO CLIENTE ', '', '2016-07-16', '13:50:29', 1),
(424, 'CADASTRO DO CLIENTE ', '', '2016-07-16', '15:39:46', 1),
(425, 'CADASTRO DO CLIENTE ', '', '2016-07-16', '15:43:43', 1),
(426, 'CADASTRO DO CLIENTE ', '', '2016-07-16', '15:51:29', 1),
(427, 'CADASTRO DO CLIENTE ', '', '2016-07-16', '15:57:59', 1),
(428, 'CADASTRO DO CLIENTE ', '', '2016-07-16', '16:10:00', 1),
(429, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '16:11:47', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_lojas`
--

CREATE TABLE `tb_lojas` (
  `idloja` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `title_google` longtext,
  `description_google` longtext,
  `keywords_google` longtext,
  `link_maps` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_lojas`
--

INSERT INTO `tb_lojas` (`idloja`, `titulo`, `telefone`, `endereco`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `description_google`, `keywords_google`, `link_maps`) VALUES
(2, 'BRASÍLIA', '(61) 3336-2400', 'EQNL 09/11, Bloco C, Loja 03  Taguatinga - Brasília -  DF', 'SIM', NULL, 'brasilia', '', '', '', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15358.24211909388!2d-47.886154!3d-15.774371!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8d8f1a03bc3610ea!2sMil+Fogos+de+Artif%C3%ADcio+Asa+Norte!5e0!3m2!1spt-BR!2sbr!4v1465682693877'),
(5, 'GOIÂNIA', '(62) 3922-2606', 'Rua 1126, Nº 60, sala 02,  Luciano Freire Center - St. Marista - Goiânia - GO', 'SIM', NULL, 'goiania', '', '', '', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15285.94674815496!2d-49.263336!3d-16.702551!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb3658254dcbbb894!2sMil+Fogos+de+Artif%C3%ADcio+Goi%C3%A2nia!5e0!3m2!1spt-BR!2sbr!4v1465682728738'),
(6, 'BRASÍLIA', '(61) 3964-1001', 'SCLN 305, bloco B, sala 105 - Brasília - DF', 'SIM', NULL, 'brasilia', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_portfolios`
--

CREATE TABLE `tb_portfolios` (
  `idportfolio` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `title_google` varchar(255) DEFAULT NULL,
  `keywords_google` longtext,
  `description_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `video_youtube` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_portfolios`
--

INSERT INTO `tb_portfolios` (`idportfolio`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `video_youtube`) VALUES
(9, 'Casamento - DF - Espaço da Corte', '1207201608534872469612..jpg', '<p>\r\n	Casamento&nbsp;Daniela e Sergio</p>\r\n<p>\r\n	Bras&iacute;lia - DF</p>\r\n<p>\r\n	Local: &nbsp;Espa&ccedil;o da Corte - Daniela e Sergio</p>', '', '', '', 'SIM', NULL, 'casamento--df--espaco-da-corte', 'https://www.youtube.com/embed/nHhhAjzSETc'),
(10, 'Casamento - DF - Espaço da Corte - Demonstração - Fernando Peixoto', '1507201609191289447599..jpg', '<p>\r\n	Casamento</p>\r\n<p>\r\n	Bras&iacute;lia - DF</p>\r\n<p>\r\n	Local: Espa&ccedil;o da Corte</p>\r\n<p>\r\n	Demonstra&ccedil;&atilde;o - Fernando Peixoto</p>', '', '', '', 'SIM', NULL, 'casamento--df--espaco-da-corte--demonstracao--fernando-peixoto', 'https://www.youtube.com/embed/O_ZbbRYxd4E'),
(11, 'Casamento - DF - Diversos - Foto - Di Souza', '1507201609244794949942..jpg', '<div>\r\n	Casamento - DF - Diversos - Foto - Di Souza</div>', '', '', '', 'NAO', NULL, 'casamento--df--diversos--foto--di-souza', ''),
(12, 'Casamento - DF - Casa Bonita', '1507201609286963392471..jpg', '<p>\r\n	Casamento</p>\r\n<p>\r\n	Bras&iacute;lia - DF</p>\r\n<p>\r\n	Local: Casa Bonita</p>', '', '', '', 'SIM', NULL, 'casamento--df--casa-bonita', 'https://www.youtube.com/embed/ArmeN4t5w5c'),
(13, 'Casamento - DF - Country House', '1507201609324311088613..jpg', '<p>\r\n	Casamento</p>\r\n<p>\r\n	Bras&iacute;lia - DF</p>\r\n<p>\r\n	Local: Country House</p>', '', '', '', 'SIM', NULL, 'casamento--df--country-house', 'https://www.youtube.com/embed/AWF6OhWqowI'),
(14, 'Casamento - DF - Imaculado Coração de Maria - Saída cerimonia', '1507201609417906591080..jpg', '<p>\r\n	Casamento - Sa&iacute;da da Cerim&ocirc;nia</p>\r\n<p>\r\n	Bras&iacute;lia - DF</p>\r\n<p>\r\n	Local: Imaculado Cora&ccedil;&atilde;o de Maria</p>', '', '', '', 'SIM', NULL, 'casamento--df--imaculado-coracao-de-maria--saida-cerimonia', 'https://www.youtube.com/embed/kJwVdkH7lJI'),
(15, 'Casamento - DF - Varandas Park', '1507201609439984070083..jpg', '<p>\r\n	Casamento</p>\r\n<p>\r\n	Bras&iacute;lia - DF</p>\r\n<p>\r\n	Local: Varandas Park</p>\r\n<p>\r\n	Efeito de gelo seco e gerb&acute;s na ponte</p>', '', '', '', 'SIM', NULL, 'casamento--df--varandas-park', 'https://www.youtube.com/embed/qXJ2QNhiQqE'),
(16, 'Eventos - DF - Entrega Brookfield', '1507201609476271277885..jpg', '<p>\r\n	Entrega Brookfield</p>\r\n<p>\r\n	Bras&iacute;lia - DF</p>', '', '', '', 'SIM', NULL, 'eventos--df--entrega-brookfield', 'https://www.youtube.com/embed/gVNFqdDCShE'),
(17, 'Fantasma da Ópera - Evento nacional', '1507201609513880027267..jpg', '<p>\r\n	Fantasma da &Oacute;pera</p>\r\n<p>\r\n	Evento nacional</p>', '', '', '', 'SIM', NULL, 'fantasma-da-opera--evento-nacional', 'https://www.youtube.com/embed/vXIicS0VHzE'),
(18, 'Aniversário - DF - Porto Vitória - 15 Anos', '1507201609531932292112..jpg', '<p>\r\n	Anivers&aacute;rio</p>\r\n<p>\r\n	Bras&iacute;lia - DF</p>\r\n<p>\r\n	Porto Vit&oacute;ria - 15 Anos</p>', '', '', '', 'SIM', NULL, 'aniversario--df--porto-vitoria--15-anos', 'https://www.youtube.com/embed/4bXjZKn-cCQ'),
(19, 'Aniversário - GO - Anápolis - 15 anos - Lune', '1507201609576739142342..jpg', '<p>\r\n	Anivers&aacute;rio</p>\r\n<p>\r\n	An&aacute;polis - GO</p>\r\n<p>\r\n	15 anos - Lune</p>', '', '', '', 'SIM', NULL, 'aniversario--go--anapolis--15-anos--lune', 'https://www.youtube.com/embed/CdGiP75lc1c'),
(20, 'Aniversário - DF - Villa Riza - 15 anos - Cascata', '1507201610002081167523..jpg', '<p>\r\n	Anivers&aacute;rio</p>\r\n<p>\r\n	Bras&iacute;lia - DF</p>\r\n<p>\r\n	Local: Villa Riza</p>\r\n<p>\r\n	15 anos - Cascata</p>', '', '', '', 'SIM', NULL, 'aniversario--df--villa-riza--15-anos--cascata', 'https://www.youtube.com/embed/xUxbQcVmoYE');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos`
--

CREATE TABLE `tb_produtos` (
  `idproduto` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title_google` longtext CHARACTER SET utf8 NOT NULL,
  `keywords_google` longtext CHARACTER SET utf8 NOT NULL,
  `description_google` longtext CHARACTER SET utf8 NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) UNSIGNED NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `src_youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao_video` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `id_categoriaproduto`, `marca`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`) VALUES
(1, 'Sputinikão Super Star c/ 2 unidades', '1507201603168550009503..jpg', '<p>\r\n	Sputinik&atilde;o Super Star</p>\r\n<p>\r\n	Caixa com 2 unidades</p>', '', '', '', 'SIM', 0, 'sputinikao-super-star-c-2-unidades', 77, NULL, NULL, NULL, '', ''),
(2, 'Foguete 12 x 1 Tiros caixa com 06 unidades', '1507201602138092670270..jpg', '<p>\r\n	Foguete 12 x 1 Tiros</p>\r\n<p>\r\n	Caixa com 06 unidades</p>', '', '', '', 'SIM', 0, 'foguete-12-x-1-tiros-caixa-com-06-unidades', 71, NULL, NULL, NULL, '', ''),
(7, 'Foguete 19 x 1 Tiros caixa com 06 unidades', '1507201602143563844259..jpg', '<p>\r\n	Foguete 19 x 1 Tiros</p>\r\n<p>\r\n	Caixa com 06 unidades</p>', '', '', '', 'SIM', 0, 'foguete-19-x-1-tiros-caixa-com-06-unidades', 71, NULL, NULL, NULL, '', ''),
(8, 'Foguete 12 x 3 Tiros e 3 apitos caixa com 06 unidades', '2903201601361221527601..jpg', '<p>\r\n	Foguete 12 x 3 Tiros e 3 apitos caixa com 06 unidades</p>', '', '', '', 'NAO', 0, 'foguete-12-x-3-tiros-e-3-apitos-caixa-com-06-unidades', 71, NULL, NULL, NULL, '', ''),
(9, 'Foguete 1 Tiro caixa com 12 unidades', '1507201602162231070655..jpg', '<p>\r\n	Foguete 1 Tiro</p>\r\n<p>\r\n	Caixa com 12 unidades</p>', '', '', '', 'SIM', 0, 'foguete-1-tiro-caixa-com-12-unidades', 71, NULL, NULL, NULL, '', ''),
(10, 'Foguete 3 Tiros caixa com 12 unidades', '1507201602186467760445..jpg', '<p>\r\n	Foguete 3 Tiros</p>\r\n<p>\r\n	Caixa com 12 unidades</p>', '', '', '', 'SIM', 0, 'foguete-3-tiros-caixa-com-12-unidades', 71, NULL, NULL, NULL, '', ''),
(11, 'Foguete 3 x 1 Tiros caixa com 12 unidades', '1507201602202988119100..jpg', '<p>\r\n	Foguete 3 x 1 Tiros</p>\r\n<p>\r\n	Caixa com 12 unidades</p>', '', '', '', 'SIM', 0, 'foguete-3-x-1-tiros-caixa-com-12-unidades', 71, NULL, NULL, NULL, '', ''),
(12, 'Foguete 7 x 1 Tiros caixa com 06 unidades', '1507201602217891153767..jpg', '<p>\r\n	Foguete 7 x 1 Tiros</p>\r\n<p>\r\n	Caixa com 06 unidades</p>', '', '', '', 'SIM', 0, 'foguete-7-x-1-tiros-caixa-com-06-unidades', 71, NULL, NULL, NULL, '', ''),
(13, 'Girândola 180 - Mista', '1507201602233132970904..jpg', '<p>\r\n	Gir&acirc;ndola 180 - Mista</p>\r\n<p>\r\n	01 Unidade</p>', '', '', '', 'SIM', 0, 'girandola-180--mista', 74, NULL, NULL, NULL, '', ''),
(14, 'Girândola 468 - Tiros', '1507201602263909903359..jpg', '<p>\r\n	Gir&acirc;ndola 468 - Tiros</p>\r\n<p>\r\n	01 Unidade</p>', '', '', '', 'SIM', 0, 'girandola-468--tiros', 74, NULL, NULL, NULL, '', ''),
(15, 'Girândola 468 - 08 Efeitos exclusiva', '1507201602268082997740..jpg', '<p>\r\n	Gir&acirc;ndola 468 - 08 Efeitos exclusiva</p>\r\n<p>\r\n	01 Unidade</p>', '', '', '', 'SIM', 0, 'girandola-468--08-efeitos-exclusiva', 74, NULL, NULL, NULL, '', ''),
(16, 'Girândola 3600 - 08 Efeitos exclusiva', '1507201602281503096574..jpg', '<p>\r\n	Gir&acirc;ndola 3600 - 08 Efeitos exclusiva</p>\r\n<p>\r\n	01 Unidade</p>', '', '', '', 'SIM', 0, 'girandola-3600--08-efeitos-exclusiva', 74, NULL, NULL, NULL, '', ''),
(17, 'Bateria de 06 Tiros - Caixa com 12 Bombas', '1507201602301360809885..jpg', '<p>\r\n	Bateria de 06 Tiros</p>\r\n<p>\r\n	Caixa com 12 Bombas</p>', '', '', '', 'SIM', 0, 'bateria-de-06-tiros--caixa-com-12-bombas', 77, NULL, NULL, NULL, '', ''),
(18, 'Bateria de 12 Tiros - Caixa com 06 Bombas', '1507201602325072456879..jpg', '<p>\r\n	Bateria de 12 Tiros</p>\r\n<p>\r\n	Caixa com 06 Bombas</p>', '', '', '', 'SIM', 0, 'bateria-de-12-tiros--caixa-com-06-bombas', 77, NULL, NULL, NULL, '', ''),
(19, 'Bateria de 18 Tiros - Caixa com 04 Bombas', '1507201602337102215434..jpg', '<p>\r\n	Bateria de 18 Tiros</p>\r\n<p>\r\n	Caixa com 04 Bombas</p>', '', '', '', 'SIM', 0, 'bateria-de-18-tiros--caixa-com-04-bombas', 77, NULL, NULL, NULL, '', ''),
(20, 'Bomba Nº 4 - Caixa com 20 unidades', '1507201602347988854257..jpg', '<p>\r\n	Bomba N&ordm; 4</p>\r\n<p>\r\n	Caixa com 20 unidades</p>', '', '', '', 'SIM', 0, 'bomba-n-4--caixa-com-20-unidades', 77, NULL, NULL, NULL, '', ''),
(21, 'Chuva de Prata Nº 6 - Caixa com 10 unidades', '1507201602365218096586..jpg', '<p>\r\n	Chuva de Prata N&ordm; 6</p>\r\n<p>\r\n	Caixa com 10 unidades</p>', '', '', '', 'SIM', 0, 'chuva-de-prata-n-6--caixa-com-10-unidades', 77, NULL, NULL, NULL, '', ''),
(22, 'Foguetes Chorão Colorido', '1507201602388664908367..jpg', '<p>\r\n	Foguetes Chor&atilde;o Colorido</p>\r\n<p>\r\n	1 Caixa</p>', '', '', '', 'SIM', 0, 'foguetes-chorao-colorido', 71, NULL, NULL, NULL, '', ''),
(23, 'Foguetão Colorido Mix', '1507201602409627139282..jpg', '<p>\r\n	Foguet&atilde;o Colorido Mix</p>\r\n<p>\r\n	01 Caixa</p>', '', '', '', 'SIM', 0, 'foguetao-colorido-mix', 71, NULL, NULL, NULL, '', ''),
(24, 'Foguete Colorido Estrela Prateada', '1507201602426646803013..jpg', '<p>\r\n	Foguete Colorido Estrela Prateada</p>\r\n<p>\r\n	01 Caixa</p>', '', '', '', 'SIM', 0, 'foguete-colorido-estrela-prateada', 71, NULL, NULL, NULL, '', ''),
(25, 'Foguete Marcius', '1507201604416682151659..jpg', '<p>\r\n	Foguete Marcius</p>\r\n<p>\r\n	01 Caixa</p>', '', '', '', 'SIM', 0, 'foguete-marcius', 71, NULL, NULL, NULL, '', ''),
(26, 'Traques', '1507201611149843704187..jpg', '<p>\r\n	Traques</p>\r\n<p>\r\n	01 Caixa</p>', '', '', '', 'SIM', 0, 'traques', 77, NULL, NULL, NULL, '', ''),
(27, 'Estalos de Salão', '1507201611157999483918..jpg', '<p>\r\n	Estalos de Sal&atilde;o</p>\r\n<p>\r\n	01 Caixa</p>', '', '', '', 'SIM', 0, 'estalos-de-salao', 77, NULL, NULL, NULL, '', ''),
(28, 'Torta Infinity Show', '1507201611169831004287..jpg', '<p>\r\n	Torta Infinity Show</p>\r\n<p>\r\n	01 Caixa</p>', '', '', '', 'SIM', 0, 'torta-infinity-show', 75, NULL, NULL, NULL, '', ''),
(29, 'Apito com vara - Pacote com 12 unidades', '1507201611189578304100..jpg', '<p>\r\n	Apito com vara</p>\r\n<p>\r\n	01 Pacote com 12 unidades</p>', '', '', '', 'SIM', 0, 'apito-com-vara--pacote-com-12-unidades', 77, NULL, NULL, NULL, '', ''),
(30, 'Pião Colorido - Caixa com 12 unidades', '1507201611201292799893..jpg', '<p>\r\n	Pi&atilde;o Colorido</p>\r\n<p>\r\n	01 Caixa com 12 unidades</p>', '', '', '', 'SIM', 0, 'piao-colorido--caixa-com-12-unidades', 77, NULL, NULL, NULL, '', ''),
(31, 'Rojão de Vara', '1507201611219837646343..jpg', '<p>\r\n	Roj&atilde;o de Vara (Diversas cores e efeitos)</p>', '', '', '', 'SIM', 0, 'rojao-de-vara', 71, NULL, NULL, NULL, '', ''),
(32, 'Bomba Faraônica - Caixa com 12 unidades', '1507201604434431040100..jpg', '<p>\r\n	Bomba Fara&ocirc;nica - Caixa com 12 unidades</p>', '', '', '', 'SIM', 0, 'bomba-faraonica--caixa-com-12-unidades', 77, NULL, NULL, NULL, '', ''),
(33, 'Torta Contratack - com 50 tubos', '1507201611237352224530..jpg', '<p>\r\n	Torta Contratack - com 50 tubos</p>', '', '', '', 'SIM', 0, 'torta-contratack--com-50-tubos', 75, NULL, NULL, NULL, '', ''),
(34, 'Girândola 1080 - Cores', '1507201611245563954193..jpg', '<p>\r\n	Gir&acirc;ndola 1080 - Cores</p>', '', '', '', 'SIM', 0, 'girandola-1080--cores', 74, NULL, NULL, NULL, '', ''),
(35, 'Foguete Treme Terra', '1507201611332116075613..jpg', '<p>\r\n	Foguete Treme Terra</p>', '', '', '', 'SIM', 0, 'foguete-treme-terra', 71, NULL, NULL, NULL, '', ''),
(36, 'Torta Sensação com 50 tubos de 1,5', '1507201604453431655011..jpg', '<p>\r\n	Torta Sensa&ccedil;&atilde;o com 50 tubos de 1,5</p>', '', '', '', 'SIM', 0, 'torta-sensacao-com-50-tubos-de-15', 75, NULL, NULL, NULL, '', ''),
(37, 'Bomba Nº 1 - Caixa com 20 unidades', '1507201611388164621231..jpg', '<p>\r\n	Bomba N&ordm; 1 - Caixa com 20 unidades</p>', '', '', '', 'SIM', 0, 'bomba-n-1--caixa-com-20-unidades', 77, NULL, NULL, NULL, '', ''),
(38, 'Cometinha Amarelo, Vermelho, Prata ou Verde com 12 unidades', '1507201611536164905052..jpg', '<p>\r\n	Cometinha Amarelo, Vermelho, Prata ou Verde com 12 unidades</p>', '', '', '', 'SIM', 0, 'cometinha-amarelo-vermelho-prata-ou-verde-com-12-unidades', 77, NULL, NULL, NULL, '', ''),
(39, 'Candela com 10 tiros (cores)', '1507201611589427194214..jpg', '<p>\r\n	Candela com 10 tiros (cores)</p>', '', '', '', 'SIM', 0, 'candela-com-10-tiros-cores', 77, NULL, NULL, NULL, '', ''),
(40, 'Candela com 20 tiros (cores)', '1507201611588349911396..jpg', '<p>\r\n	Candela com 20 tiros (cores)</p>', '', '', '', 'SIM', 0, 'candela-com-20-tiros-cores', 77, NULL, NULL, NULL, '', ''),
(41, 'Candela com 30 tiros (cores)', '1507201611587774883301..jpg', '<p>\r\n	Candela com 30 tiros (cores)</p>', '', '', '', 'SIM', 0, 'candela-com-30-tiros-cores', 77, NULL, NULL, NULL, '', ''),
(42, 'Sputinikão com 2 unidades', '1507201612033038243506..jpg', '<p>\r\n	Sputinik&atilde;o com 2 unidades</p>', '', '', '', 'SIM', 0, 'sputinikao-com-2-unidades', 77, NULL, NULL, NULL, '', ''),
(43, 'Arvore de Natal com 6 unidades', '1507201612067234779567..jpg', '<p>\r\n	Arvore de Natal com 6 unidades</p>', '', '', '', 'SIM', 0, 'arvore-de-natal-com-6-unidades', 77, NULL, NULL, NULL, '', ''),
(44, 'Torta Mistica com 100 tubos de 1,5', '1607201601412810186884..jpg', '<p>\r\n	Torta Mistica com 100 tubos de 1,5</p>', '', '', '', 'SIM', 0, 'torta-mistica-com-100-tubos-de-15', 75, NULL, NULL, NULL, '', ''),
(45, 'Minhoquinha Maluca com 20 unidades', '1507201612111111421195..jpg', '<p>\r\n	Minhoquinha Maluca com 20 unidades</p>', '', '', '', 'SIM', 0, 'minhoquinha-maluca-com-20-unidades', 77, NULL, NULL, NULL, '', ''),
(46, 'Bastão de Crakling e Cores com 4 unidades', '1507201612137639712033..jpg', '<p>\r\n	Bast&atilde;o de Crakling e Cores com 4 unidades</p>', '', '', '', 'SIM', 0, 'bastao-de-crakling-e-cores-com-4-unidades', 77, NULL, NULL, NULL, '', ''),
(47, 'Vulcão Craker, Craker/Cores e Estrela Prateada com 2 unidades', '1507201612231450723868..jpg', '<p>\r\n	Vulc&atilde;o Craker, Craker/Cores e Estrela Prateada com 2 unidades</p>', '', '', '', 'SIM', 0, 'vulcao-craker-crakercores-e-estrela-prateada-com-2-unidades', 77, NULL, NULL, NULL, '', ''),
(48, 'Torta em Leque com 72 tubos de 1,5', '1607201601423246352651..jpg', '<p>\r\n	Torta em Leque com 72 tubos de 1,5</p>', '', '', '', 'SIM', 0, 'torta-em-leque-com-72-tubos-de-15', 75, NULL, NULL, NULL, '', ''),
(49, 'Torta Leque de cores Z e W com 120 tubos de 3/4', '1507201612416669074114..jpg', '<p>\r\n	Torta Leque de cores Z e W com 120 tubos de 3/4</p>', '', '', '', 'SIM', 0, 'torta-leque-de-cores-z-e-w-com-120-tubos-de-34', 75, NULL, NULL, NULL, '', ''),
(50, 'Aero Musical com 6 unidades', '1607201601446221748213..jpg', '<p>\r\n	Aero Musical com 6 unidades</p>', '', '', '', 'SIM', 0, 'aero-musical-com-6-unidades', 77, NULL, NULL, NULL, '', ''),
(51, 'Torta 3D com 25 tubos de 2,5"', '1507201612516777855186..jpg', '<p>\r\n	Torta 3D com 25 tubos de 2,5&quot;</p>', '', '', '', 'SIM', 0, 'torta-3d-com-25-tubos-de-25', 75, NULL, NULL, NULL, '', ''),
(52, 'Torta Picasso com 81 tubos de 3/4', '1507201601029634399803..jpg', '<p>\r\n	Torta Picasso com 81 tubos de 3/4</p>', '', '', '', 'SIM', 0, 'torta-picasso-com-81-tubos-de-34', 75, NULL, NULL, NULL, '', ''),
(53, 'Torta Cancun e Miami com 100 tubos de 1" em leque', '1507201601091295370745..jpg', '<p>\r\n	Torta Cancun e Miami com 100 tubos de 1&quot; em leque</p>', '', '', '', 'SIM', 0, 'torta-cancun-e-miami-com-100-tubos-de-1-em-leque', 75, NULL, NULL, NULL, '', ''),
(54, 'Torta Athos, Aramis e Dartagnan com 25 tubos de 1,5"', '1507201601135615153759..jpg', '<p>\r\n	Torta Athos, Aramis e Dartagnan com 25 tubos de 1,5&quot;</p>', '', '', '', 'SIM', 0, 'torta-athos-aramis-e-dartagnan-com-25-tubos-de-15', 75, NULL, NULL, NULL, '', ''),
(55, 'Base Míssil com 25', '1507201601168602901221..jpg', '<p>\r\n	Base M&iacute;ssil com 25</p>', '', '', '', 'SIM', 0, 'base-missil-com-25', 77, NULL, NULL, NULL, '', ''),
(56, 'Base Míssil com 50', '1507201601189529463383..jpg', '<p>\r\n	Base M&iacute;ssil com 50</p>', '', '', '', 'SIM', 0, 'base-missil-com-50', 77, NULL, NULL, NULL, '', ''),
(57, 'Base Míssil com 200', '1607201601465968037740..jpg', '<p>\r\n	Base M&iacute;ssil com 200</p>', '', '', '', 'SIM', 0, 'base-missil-com-200', 77, NULL, NULL, NULL, '', ''),
(58, 'Avião Noturno com 12 unidades', '1507201601217851021434..jpg', '<p>\r\n	Avi&atilde;o Noturno com 12 unidades</p>', '', '', '', 'SIM', 0, 'aviao-noturno-com-12-unidades', 77, NULL, NULL, NULL, '', ''),
(59, 'Fumaça Colorida (Lata Grande 3")', '1507201604577865501898..jpg', '<p>\r\n	Fuma&ccedil;a Colorida (Lata Grande 3&quot;)</p>', '', '', '', 'SIM', 0, 'fumaca-colorida-lata-grande-3', 76, NULL, NULL, NULL, '', ''),
(60, 'Fumaça Colorida (Lata Gigante 3")', '1507201604579925659191..jpg', '<p>\r\n	Fuma&ccedil;a Colorida (Lata Gigante 3&quot;)</p>', '', '', '', 'SIM', 0, 'fumaca-colorida-lata-gigante-3', 76, NULL, NULL, NULL, '', ''),
(61, 'Fumaça Colorida (Lata pequena 1,5")', '1507201604587023758259..jpg', '<p>\r\n	Fuma&ccedil;a Colorida (Lata pequena 1,5&quot;)</p>', '', '', '', 'SIM', 0, 'fumaca-colorida-lata-pequena-15', 76, NULL, NULL, NULL, '', ''),
(62, 'Torta VIP com 112 tubos de 1,5 e 2,5', '1507201605005391348683..jpg', '<p>\r\n	Torta VIP com 112 tubos de 1,5 e 2,5</p>', '', '', '', 'SIM', 0, 'torta-vip-com-112-tubos-de-15-e-25', 75, NULL, NULL, NULL, '', ''),
(63, 'Torta ZEUS com 64 Tubos 3/4"', '1507201605037551495888..jpg', '<p>\r\n	Torta ZEUS com 64 Tubos 3/4&quot;</p>', '', '', '', 'SIM', 0, 'torta-zeus-com-64-tubos-34', 75, NULL, NULL, NULL, '', ''),
(64, 'Torta IZIS com 25 Tubos de 1,5', '1507201605052632808138..jpg', '<p>\r\n	Torta IZIS com 25 Tubos de 1,5</p>', '', '', '', 'SIM', 0, 'torta-izis-com-25-tubos-de-15', 75, NULL, NULL, NULL, '', ''),
(65, 'Foguete 19 x 4 Tiros caixa com 06 unidades', '1607201601505300407757..jpg', '<p>\r\n	Foguete 19 x 4 Tiros</p>\r\n<p>\r\n	01 Caixa com 06 unidades</p>', '', '', '', 'SIM', 0, 'foguete-19-x-4-tiros-caixa-com-06-unidades', 71, NULL, NULL, NULL, '', ''),
(66, 'Torta Jumbo com 100 Tubos de 1,5', '1607201603392581268412..jpg', '<p>\r\n	Torta Jumbo&nbsp;</p>\r\n<p>\r\n	01 Caixa&nbsp;com 100 Tubos de 1,5</p>', '', '', '', 'SIM', 0, 'torta-jumbo-com-100-tubos-de-15', 75, NULL, NULL, NULL, '', ''),
(67, 'Peido Alemão', '1607201603431595441173..jpg', '<p>\r\n	Peido Alem&atilde;o</p>\r\n<p>\r\n	01 Unidade</p>', '', '', '', 'SIM', 0, 'peido-alemao', 77, NULL, NULL, NULL, '', ''),
(68, 'Torta Tremendão Cores com 16 Tubos', '1607201603514747049742..jpg', '<p>\r\n	Torta Tremend&atilde;o Cores</p>\r\n<p>\r\n	01 Caixa com 16 Tubos</p>', '', '', '', 'SIM', 0, 'torta-tremendao-cores-com-16-tubos', 75, NULL, NULL, NULL, '', ''),
(69, 'Torta Tremendão Cores com 50 Tubos de 1,8', '1607201603572150797624..jpg', '<p>\r\n	Torta Tremend&atilde;o Cores</p>\r\n<p>\r\n	01 Caixa com 50 Tubos de 1,8</p>', '', '', '', 'SIM', 0, 'torta-tremendao-cores-com-50-tubos-de-18', 75, NULL, NULL, NULL, '', ''),
(70, 'Busca Pé com 10 unidades', '1607201604117945153878..jpg', '<p>\r\n	Busca P&eacute;</p>\r\n<p>\r\n	01 Caixa com 10 unidades</p>', '', '', '', 'SIM', 0, 'busca-pe-com-10-unidades', 77, NULL, NULL, NULL, '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_seo`
--

CREATE TABLE `tb_seo` (
  `idseo` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `title_google` longtext,
  `description_google` longtext,
  `keywords_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_seo`
--

INSERT INTO `tb_seo` (`idseo`, `titulo`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'Index', 'Index title', 'Index description', 'Index Keywords', 'SIM', NULL, 'index'),
(2, 'Empresa', 'Empresa title', NULL, NULL, 'SIM', NULL, NULL),
(6, 'Portfólio', 'Portfólio title', 'Portfólio description', 'Portfólio keywords', 'SIM', NULL, NULL),
(10, 'Dicas', 'Dicas Title', NULL, NULL, 'SIM', NULL, NULL),
(11, 'Fale conosco', 'Fale conosco title', NULL, NULL, 'SIM', NULL, NULL),
(12, 'Trabalhe Conosco', 'Trabalhe Conosco Title', NULL, NULL, 'SIM', NULL, NULL),
(13, 'Produtos', 'Produtos Title', NULL, NULL, 'SIM', NULL, NULL),
(14, 'Orçamento', 'Orçamento title', NULL, NULL, 'SIM', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_servicos`
--

CREATE TABLE `tb_servicos` (
  `idservico` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_categoriaservico` int(10) UNSIGNED NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  ADD PRIMARY KEY (`idavaliacaoproduto`);

--
-- Indexes for table `tb_banners`
--
ALTER TABLE `tb_banners`
  ADD PRIMARY KEY (`idbanner`);

--
-- Indexes for table `tb_banners_internas`
--
ALTER TABLE `tb_banners_internas`
  ADD PRIMARY KEY (`idbannerinterna`);

--
-- Indexes for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  ADD PRIMARY KEY (`idcategoriaproduto`);

--
-- Indexes for table `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  ADD PRIMARY KEY (`idcomentariodica`);

--
-- Indexes for table `tb_comentarios_produtos`
--
ALTER TABLE `tb_comentarios_produtos`
  ADD PRIMARY KEY (`idcomentarioproduto`);

--
-- Indexes for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  ADD PRIMARY KEY (`idconfiguracao`);

--
-- Indexes for table `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  ADD PRIMARY KEY (`iddepoimento`);

--
-- Indexes for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  ADD PRIMARY KEY (`iddica`);

--
-- Indexes for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  ADD PRIMARY KEY (`idempresa`);

--
-- Indexes for table `tb_especificacoes`
--
ALTER TABLE `tb_especificacoes`
  ADD PRIMARY KEY (`idespecificacao`);

--
-- Indexes for table `tb_galerias_portifolios`
--
ALTER TABLE `tb_galerias_portifolios`
  ADD PRIMARY KEY (`idgaleriaportifolio`);

--
-- Indexes for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  ADD PRIMARY KEY (`id_galeriaproduto`);

--
-- Indexes for table `tb_logins`
--
ALTER TABLE `tb_logins`
  ADD PRIMARY KEY (`idlogin`);

--
-- Indexes for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  ADD PRIMARY KEY (`idloglogin`);

--
-- Indexes for table `tb_lojas`
--
ALTER TABLE `tb_lojas`
  ADD PRIMARY KEY (`idloja`);

--
-- Indexes for table `tb_portfolios`
--
ALTER TABLE `tb_portfolios`
  ADD PRIMARY KEY (`idportfolio`);

--
-- Indexes for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  ADD PRIMARY KEY (`idproduto`);

--
-- Indexes for table `tb_seo`
--
ALTER TABLE `tb_seo`
  ADD PRIMARY KEY (`idseo`);

--
-- Indexes for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  ADD PRIMARY KEY (`idservico`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  MODIFY `idavaliacaoproduto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_banners`
--
ALTER TABLE `tb_banners`
  MODIFY `idbanner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_banners_internas`
--
ALTER TABLE `tb_banners_internas`
  MODIFY `idbannerinterna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  MODIFY `idcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  MODIFY `idcomentariodica` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_comentarios_produtos`
--
ALTER TABLE `tb_comentarios_produtos`
  MODIFY `idcomentarioproduto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  MODIFY `idconfiguracao` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  MODIFY `iddepoimento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  MODIFY `iddica` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  MODIFY `idempresa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_especificacoes`
--
ALTER TABLE `tb_especificacoes`
  MODIFY `idespecificacao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_galerias_portifolios`
--
ALTER TABLE `tb_galerias_portifolios`
  MODIFY `idgaleriaportifolio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  MODIFY `id_galeriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `tb_logins`
--
ALTER TABLE `tb_logins`
  MODIFY `idlogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  MODIFY `idloglogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=430;
--
-- AUTO_INCREMENT for table `tb_lojas`
--
ALTER TABLE `tb_lojas`
  MODIFY `idloja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_portfolios`
--
ALTER TABLE `tb_portfolios`
  MODIFY `idportfolio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  MODIFY `idproduto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `tb_seo`
--
ALTER TABLE `tb_seo`
  MODIFY `idseo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  MODIFY `idservico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
