<div class="clearfix"></div>
<div class="container-fluid rodape">
	<div class="row">

		<!-- menu -->
		<div class="container top30">
			<div class="row">
				<div class="col-xs-12 bg-menu-rodape">
					<ul class="menu-rodape">
						<li class="active"><a href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
						<li><a href="<?php echo Util::caminho_projeto() ?>/empresa">EMPRESA</a></li>
						<?php /* ?><li><a href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a></li><?php */ ?>
						<li><a href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS</a></li>
						<li><a href="<?php echo Util::caminho_projeto() ?>/eventos">EVENTOS</a></li>
						<li><a href="<?php echo Util::caminho_projeto() ?>/fale-conosco">CONTATOS</a></li>
					</ul>
				</div>

				<!-- menu -->

				<!-- logo, endereco, telefone -->
				<div class="col-xs-12 top15 bottom20">
						<div class="col-xs-2 bottom20">
							<a href="<?php echo Util::caminho_projeto() ?>">
								<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-rodape.png" alt="">
							</a>
						</div>

						<div class="col-xs-6 contatos_rodape top20">

							  <?php
							  $result = $obj_site->select("tb_lojas", "limit 2");
							  if (mysql_num_rows($result) > 0) {
							    while($row = mysql_fetch_array($result)){
							    	$enderecos[] = $row;
							    ?>
									<p class="bottom15"><i class="fa fa-home"></i><?php Util::imprime($row[endereco]); ?></p>
							    <?php
							    }
							  }
							  ?>
						</div>

						<div class="col-xs-2 contatos_rodape top20">
							<?php if (count($enderecos) > 0) {
								foreach ($enderecos as $key => $endereco) {
								?>
									<p class="bottom15"><i class="fa fa-phone"></i><?php Util::imprime($endereco[telefone]); ?></p>
								<?php
								}
							} ?>
						</div>

						<div class="col-xs-2 top20 text-right">

							<?php if ($config[google_plus] != "") { ?>
								<a class="pull-left top25 left50" href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
									<p class=""><i class="fa fa-google-plus"></i></p>
								</a>
							<?php } ?>

							<a href="http://www.homewebbrasil.com.br" target="_blank">
								<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-masmidia.png"  alt="">
							</a>
						</div>
				</div>
				<!-- logo, endereco, telefone -->

			</div>
		</div>
	</div>
</div>


<div class="container-fluid rodape-preto">
	<div class="row">
			<div class="col-xs-12 text-center bottom15">
					<h5>© Copyright MIL FOGOS</h5>
			</div>
	</div>
</div>
