<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>

<body class="bg-empresa">
	<?php require_once('../includes/topo.php'); ?>



	
  <!--  ==============================================================  -->
  <!--- titulo bg empresa-->
  <!--  ==============================================================  --> 
  <div class="container top210 ">
    <div class="row">
      <div class="col-xs-12 text-center ">
        <h6>CONHEÇA NOSSA EMPRESA</h6>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- - titulo bg  empresa-->
  <!--  ==============================================================  -->



  <!--  ==============================================================  -->
  <!--  DESCRICAO EMPRESA-->
  <!--  ==============================================================  -->
  <div class="container top100 bottom80">
    <div class="row">
      <div class="col-xs-12 descricao">
        <div class="descricao-empresa">
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3) ?>
          <p><?php Util::imprime($row[descricao]); ?></p>
        </div>
      </div>
    </div>
  </div> 
  <!--  ==============================================================  -->
  <!-- DESCRICAO EMPRESA-->
  <!--  ==============================================================  --> 




  <?php require_once('../includes/rodape.php'); ?>

</body>

</html>




<?php require_once("../includes/js_css.php"); ?>
