<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 10);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>


  <!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  --> 
  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 12) ?>
  <style>
      .bg-interna{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
      }
  </style>
  
</head>



<body class="bg-interna">


	<?php require_once('../includes/topo.php'); ?>



	
  <!--  ==============================================================  -->
  <!--- titulo bg dicas-->
  <!--  ==============================================================  --> 
  <div class="container">
    <div class="row img-interna">
      <div class="col-xs-12 text-center">
        <h6><?php Util::imprime($banner[legenda]); ?></h6>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- - titulo bg  dicas-->
  <!--  ==============================================================  -->


  
  <!--  ==============================================================  -->
  <!--   barra pesquisas e categorias -->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="fundo-cinza effect2">
          
        <div class="col-xs-12 top15">
          
         <form action="<?php echo Util::caminho_projeto() ?>/mobile/produtos/" method="post"> 
           <div class="input-group input-group-lg">
            <input type="text" class="form-control form" placeholder="QUAL PRODUTO ESTÁ PROCURANDO?" name="busca_topo">
            <span class="input-group-btn">
              <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
            </span>
          </div>       
        </form>

      </div>

    </div>
  </div>
</div>
</div>
<!--  ==============================================================  -->
<!--   barra pesquisas e categorias -->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!-- NOSSOS DICAS HOME -->
<!--  ==============================================================  -->
<div class="container top20 nossos-dicas bottom20">
  <div class="row">
  
    <?php 
    $result = $obj_site->select("tb_dicas", $complemento);
    if (mysql_num_rows($result) > 0) {
      $i = 0;
      while($row = mysql_fetch_array($result)){
      ?>
          <!-- dica 01 -->
          <div class="col-xs-4 dicas-home">
            <div class="thumbnail padding0">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 129, 116, array("class"=>"", "alt"=>"$row[titulo]")) ?>
              </a>
              <div class="caption text-center">
                <p><?php Util::imprime($row[titulo]); ?></p>
              </div>
            </div>
          </div>
          <!-- dica 01 -->
      <?php 
          if($i == 2){
            echo '<div class="clearfix"></div>';
            $i = 0;
          }else{
            $i++;
          }
      }
    }
    ?>






</div>
</div>  

<!--  ==============================================================  -->
<!-- NOSSOS DICAS HOME -->
<!--  ==============================================================  -->




<?php require_once('../includes/rodape.php'); ?>

</body>

</html>






<?php require_once("../includes/js_css.php"); ?>
