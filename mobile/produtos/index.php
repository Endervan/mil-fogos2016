<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 13);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



<!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  --> 
  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 10) ?>
  <style>
      .bg-interna{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
      }
  </style>
  
</head>



<body class="bg-interna">



	<?php require_once('../includes/topo.php'); ?>



	
  <!--  ==============================================================  -->
  <!--- titulo bg empresa-->
  <!--  ==============================================================  --> 
  <div class="container">
    <div class="row img-interna">
      <div class="col-xs-12 text-center">
        <h6><?php Util::imprime($banner[legenda]); ?></h6>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- - titulo bg  empresa-->
  <!--  ==============================================================  -->


  
  
  <!--  ==============================================================  -->
  <!-- NOSSOS PRODUTOS -->
  <!--  ==============================================================  -->
  <div class="container bottom30">
    <div class="row">
      <div class="fundo-azul effect2 bottom30">
        <div class="col-xs-7 top5 ">
          <h5><span>NOSSOS PRODUTOS</span></h5>
        </div>
        <div class="col-xs-5">
          <form action="<?php echo Util::caminho_projeto() ?>/mobile/produtos/" method="post">
            <div class="top5">       
             <div class="input-group">
              <input type="text" class="form-control form" placeholder="BUSCAR PRODUTOS" name="busca_topo">
              <span class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </div>
          </form>
      </div>
      <!-- menu produtos-->
      <div class="col-xs-12 top5 text-center">
        <ul class="nav nav-pills">
          <li role="presentation" class="active">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos" title="VER TODOS">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-produt1.png" height="19" width="19" alt="">VER TODOS
            </a>
          </li>
          <?php 
            $result = $obj_site->select("tb_categorias_produtos");
            if (mysql_num_rows($result) > 0) {
              while($row = mysql_fetch_array($result)){
              ?>
                <li role="presentation" class="">
                  <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" height="19" width="19"><?php Util::imprime($row[titulo]); ?>
                  </a>
                </li>
              <?php 
              }
            }
            ?>
        </ul>

      </div>
      <!-- menu produtos-->
    </div>


  
    <?php 

    //  VERIFICO SE FOI SELECIONADO A CATEGORIA
    if(isset($_GET[get1]) and $_GET[get1] != '' ){
        $idcategoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $_GET[get1]);
        $complemento = "AND id_categoriaproduto = '$idcategoria'";
    }


     //  VERIFICO SE FOI SELECIONADO A CATEGORIA
    if(isset($_POST[busca_topo]) and $_POST[busca_topo] != '' ){
        $complemento = "AND titulo LIKE '%$_POST[busca_topo]%' ";
    }



    $result = $obj_site->select("tb_produtos", $complemento);
    if (mysql_num_rows($result) == 0) {
       ?>
        <div class="col-xs-12  alert alert-danger" role="alert">
          <h1 class="">Nenhum produto encontrado.</h1> 
        </div>
      <?php
    }else{
      while($row = mysql_fetch_array($result)){
      ?>
          <!-- produto01 -->
          <div class="col-xs-6 produtos-home">
            <div class="thumbnail padding0">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 208, 120, array("class"=>"", "alt"=>"$row[titulo]")) ?>
              </a>
              <div class="caption text-center">
                <h2><?php Util::imprime($row[titulo]); ?></h2>
                <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn-default btn-transparente-produto top5" role="button">
                  SAIBA MAIS
                </a>
              </div>
            </div>
          </div>
          <!-- produto01 -->
      <?php 
      }
    }
    ?>



  </div>
</div>
<!--  ==============================================================  -->
<!-- NOSSOS PRODUTOS -->
<!--  ==============================================================  -->





<?php require_once('../includes/rodape.php'); ?>

</body>

</html>



<?php require_once("../includes/js_css.php"); ?>

