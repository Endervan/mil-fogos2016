
<div class="topo-site">

  <!--  ==============================================================  -->
  <!-- telefones topo-->
  <!--  ==============================================================  -->
  <div class="container top5">
    <div class="row">

      <div class="col-xs-2"></div>

      <?php
      $result = $obj_site->select("tb_lojas", "limit 2");
      if (mysql_num_rows($result) > 0) {
        while($row = mysql_fetch_array($result)){
        ?>
          <div class="col-xs-5 text-right">
            <h3><?php Util::imprime($row[titulo]); ?></h3>
            <h3><span><?php Util::imprime($row[telefone]); ?></span></h3>
            <a href="tel:+55<?php Util::imprime($row[telefone]); ?>" class="btn btn-default btn-topo"><h3>CHAMAR</h3></a>
          </div>
        <?php
        }
      }
      ?>


    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- telefones topo-->
  <!--  ==============================================================  -->



  <!--  ==============================================================  -->
  <!-- menu-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row fundo-menu top5">
      <!-- logo -->
      <div class="col-xs-3 ">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="">
        </a>
      </div>
      <!-- logo -->

      <!-- menu-topo -->
      <div class="col-xs-6">
        <div class=" dropdown top15  right5 left30">
          <a id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-menu">
            <i class="fa fa-bars right10"></i>
            NOSSO MENU
            <i class="fa fa-chevron-down left10"></i>
          </a>
          <ul class="dropdown-menu sub-menu " aria-labelledby="dLabel">
            <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/">HOME</a></li>
            <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa">EMPRESA</a></li>
            <?php /* ?><li><a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">PRODUTOS</a></li><?php */ ?>
            <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/eventos">EVENTOS</a></li>
            <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/dicas">DICAS</a></li>
            <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/fale-conosco">CONTATOS</a></li>
            <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/trabalhe-conosco">TRABALHE CONOSCO</a></li>
          </ul>
        </div>
      </div>
      <!-- menu-topo -->



      <!-- ======================================================================= -->
      <!-- botao carrinho de compra -->
      <!-- ======================================================================= -->
      <div class=" col-xs-3 dropdown text-right">
        <a href="#" class="dropdown-toggle btn btn-azul-escuro" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-shopping-cart right10"></i>
          <span class="caret"></span></a>

          <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dLabel">
           <?php

           if(count($_SESSION[solicitacoes_produtos]) > 0)
           {
            echo '<h6 class="bottom20">MEUS PRODUTOS('. count($_SESSION[solicitacoes_produtos]) .')</h6>';

            for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
            {
              $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
              ?>
              <div class="lista-itens-carrinho col-xs-12">
                <div class="col-xs-2">
                  <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="46" width="29" alt="">
                </div>
                <div class="col-xs-8">
                  <h1><?php Util::imprime($row[titulo]) ?></h1>
                </div>
                <div class="col-xs-1">
                  <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamentos/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                </div>
              </div>
              <?php
            }
          }

          ?>



          <div class="text-right bottom20">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamentos" title="Finalizar" class="btn btn-azul-escuro1" >
              <h3>FINALIZAR</h3>
            </a>
          </div>
        </div>
      </div>
      <!-- ======================================================================= -->
      <!-- botao carrinho de compra -->
      <!-- ======================================================================= -->




    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- menu-->
  <!--  ==============================================================  -->


</div>
