<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();


// INTERNA DE PRODUTOS
$id =$_GET[get1];


$complemento = "AND url_amigavel = '$id'";


$result = $obj_site->select("tb_produtos",$complemento);
if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/mobile/produtos/");
}

$dados_dentro = mysql_fetch_array($result);


// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



<!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  --> 
  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 11) ?>
  <style>
      .bg-interna{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
      }
  </style>
  
</head>



<body class="bg-interna">



	<?php require_once('../includes/topo.php'); ?>



	
  <!--  ==============================================================  -->
  <!--- titulo bg empresa-->
  <!--  ==============================================================  --> 
  <div class="container">
    <div class="row img-interna">
      <div class="col-xs-12 text-center">
        <h6><?php Util::imprime($banner[legenda]); ?></h6>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- - titulo bg  empresa-->
  <!--  ==============================================================  -->


  
  
  <!--  ==============================================================  -->
  <!-- NOSSOS PRODUTOS -->
  <!--  ==============================================================  -->
  <div class="container bottom30">
    <div class="row">
      <div class="fundo-azul effect2 bottom30">
        <div class="col-xs-7 top5 ">
          <h5><span>NOSSOS PRODUTOS</span></h5>
        </div>
        <div class="col-xs-5">
          <form action="<?php echo Util::caminho_projeto() ?>/mobile/produtos/" method="post">
            <div class="top5">       
             <div class="input-group">
              <input type="text" class="form-control form" placeholder="BUSCAR PRODUTOS" name="busca_topo">
              <span class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </div>
          </form>
      </div>
      <!-- menu produtos-->
      <div class="col-xs-12 top5 text-center">
        <ul class="nav nav-pills">
          <li role="presentation" class="active">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos" title="VER TODOS">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-produt1.png" height="19" width="19" alt="">VER TODOS
            </a>
          </li>
          <?php 
            $result = $obj_site->select("tb_categorias_produtos");
            if (mysql_num_rows($result) > 0) {
              while($row = mysql_fetch_array($result)){
              ?>
                <li role="presentation" class="">
                  <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" height="19" width="19"><?php Util::imprime($row[titulo]); ?>
                  </a>
                </li>
              <?php 
              }
            }
            ?>
        </ul>

      </div>
      <!-- menu produtos-->
    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!-- NOSSOS PRODUTOS -->
<!--  ==============================================================  -->


<!--  ==============================================================  -->
<!--  PRODUTOS DENTRO DESCRICAO-->
<!--  ==============================================================  -->
<div class="container top200">
  <div class="row produto-dentro">
    <div class="col-xs-12">
      <h1><?php Util::imprime($dados_dentro[titulo]); ?></h1>

      <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]", 446, 281, array("class"=>"top15", "alt"=>"$dados_dentro[titulo]")) ?>

      <div class="top20">
        <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
      </div>
      
      <?php if(!empty($dados_dentro[preco])): ?>
      <div class="top20">
        <p><strong>Valor:</strong> R$ <?php echo Util::formata_moeda($dados_dentro[preco]); ?></p>
      </div>
      <?php endif; ?>


      
      <a class="btn btn-roxo top25" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">
         SOLICITAR UM ORÇAMENTO
       </a>


    </div>

    <div class="clearfix "></div>



    <!-- descricao video -->
    <?php if (!empty($dados_dentro[src_youtube])): ?>
      <div class="col-xs-9 col-xs-offset-3 text-right top25">
        <h1><strong>CONFIRA O VÍDEO DE APRESENTAÇÃO DO PRODUTO</strong></h1>
      </div>
      <!--  video -->
      <div class="col-xs-12 top10">
        <iframe width="100%" height="250" src="<?php Util::imprime($dados_dentro[src_youtube]); ?>" frameborder="0" allowfullscreen></iframe>

         <div class="top20">
          <p><?php Util::imprime($dados_dentro[descricao_video]); ?></p>
        </div>

      </div>
  <?php endif; ?>
  <!-- descricao video -->


</div>
</div>
<!--  ==============================================================  -->
<!--  PRODUTOS DENTRO DESCRICAO-->
<!--  ==============================================================  -->




<!--  ==============================================================  -->
<!-- veja tambem-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row">
    <div class="col-xs-12 top20 bottom20">
      <h4>VEJA TAMBÉM</h4>
    </div>

    <?php 
    $result = $obj_site->select("tb_produtos", "order by rand() limit 2");
    if (mysql_num_rows($result) > 0) {
      while($row = mysql_fetch_array($result)){
      ?>
          <!-- produto01 -->
          <div class="col-xs-6 produtos-home">
            <div class="thumbnail padding0">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 208, 120, array("class"=>"", "alt"=>"$row[titulo]")) ?>
              </a>
              <div class="caption text-center">
                <h2><?php Util::imprime($row[titulo]); ?></h2>
                <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn-default btn-transparente-produto top5" role="button">
                  SAIBA MAIS
                </a>
              </div>
            </div>
          </div>
          <!-- produto01 -->
      <?php 
      }
    }
    ?>


  </div>
</div> 
<!--  ==============================================================  -->
<!-- veja tambem-->
<!--  ==============================================================  --> 





<?php require_once('../includes/rodape.php'); ?>

</body>

</html>





<?php require_once("../includes/js_css.php"); ?>

