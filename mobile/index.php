<?php
require_once("../class/Include.class.php");
$obj_site = new Site();



// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>


</head>

<body>
  <?php require_once('./includes/topo.php'); ?>



  <!-- ======================================================================= -->
  <!-- slider -->
  <!-- ======================================================================= -->  
  <div class="container">
    <div class="row slider-index">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

          
          <?php
          $result = $obj_site->select("tb_banners", "and tipo_banner = 2 ");
           if(mysql_num_rows($result) > 0){
            $i = 0;
             while ($row = mysql_fetch_array($result)) {
             ?> 
                <div class="item <?php if($i == 0){ echo "active"; } ?>">
                  <img class="<?php echo $i ?>-slide" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[legenda]); ?>">
                  <div class="carousel-caption">
                   <!--  ==============================================================  -->
                   <!-- comentario e orcamento-->
                   <!--  ==============================================================  -->
                   <h5><?php Util::imprime($row[legenda]); ?></h5>
                   <?php if (!empty($row[url])) { ?>
                      <a class="btn btn-azul" href="<?php Util::imprime($row[url]); ?>" role="button">SAIBA MAIS</a>
                   <?php } ?>
                 </div>
               </div>
             <?php 
              $i++;
             }
          }
          ?>
          




     </div>

     <!-- Controls -->
      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>


   </div>
 </div>
</div>
<!-- ======================================================================= -->
<!-- slider -->
<!-- ======================================================================= -->


<!--  ==============================================================  -->
<!-- NOSSOS TRABALHOS HOME -->
<!--  ==============================================================  -->
<div class="container bg-nosso-trabalho">
  <div class="row">
    <div class="col-xs-12 top30">
      <h4>NOSSOS TRABALHOS</h4>
    </div>

    <?php 
    $result = $obj_site->select("tb_portfolios", "and imagem <> '' order by rand() limit 5" );
    if (mysql_num_rows($result) > 0) {
      while($row = mysql_fetch_array($result)){
        $dados[] = $row;
      }
    }
    ?>


    <div class="col-xs-3">
      <div class="top10">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/evento/<?php Util::imprime($dados[0][url_amigavel]); ?>" data-toggle="tooltip" data-placement="top" title="<?php Util::imprime($dados[0][titulo]); ?>">
          <?php $obj_site->redimensiona_imagem("../uploads/".$dados[0][imagem]."", 86, 78, array("class"=>"", "alt"=>"".$dados[0][titulo]."")) ?>
        </a>
      </div>

      <div class="top10">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/evento/<?php Util::imprime($dados[1][url_amigavel]); ?>" data-toggle="tooltip" data-placement="top" title="<?php Util::imprime($dados[1][titulo]); ?>">  
          <?php $obj_site->redimensiona_imagem("../uploads/".$dados[1][imagem]."", 86, 78, array("class"=>"", "alt"=>"".$dados[1][titulo]."")) ?>
        </a>         
      </div>

    </div>
    <div class="col-xs-5 top10">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/evento/<?php Util::imprime($dados[2][url_amigavel]); ?>" data-toggle="tooltip" data-placement="top" title="<?php Util::imprime($dados[2][titulo]); ?>">
        <?php $obj_site->redimensiona_imagem("../uploads/".$dados[2][imagem]."", 185, 162, array("class"=>"", "alt"=>"".$dados[2][titulo]."")) ?>
      </a>
    </div>

    <div class="clearfix"></div>
    <div class="col-xs-12 top50">
      <h4 class="bottom10">CONHEÇA MAIS A MIL FOGOS</h4>
      <?php $dados = $obj_site->select_unico("tb_empresa", "idempresa", 4) ?>
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa" title="">
          <p><?php Util::imprime($dados[descricao]); ?></p>
        </a>
    </div>

  </div>
</div>
<!--  ==============================================================  -->
<!-- NOSSOS TRABALHOS HOME -->
<!--  ==============================================================  -->




<?php /* ?>
<!--  ==============================================================  -->
<!-- NOSSOS PRODUTOS -->
<!--  ==============================================================  -->

<div class="container top50">
  <div class="row">
    <div class="fundo-azul effect2 bottom30">
      <div class="col-xs-7 top5 ">
        <h5><span>NOSSOS PRODUTOS</span></h5>
      </div>
      <div class="col-xs-5">
        <form action="<?php echo Util::caminho_projeto() ?>/mobile/produtos/" method="post">
        <div class="top5">       
         <div class="input-group">
          <input type="text" class="form-control form" placeholder="BUSCAR PRODUTOS" name="busca_topo">
          <span class="input-group-btn">
            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
          </span>
        </div>
      </div>
      </form>
    </div>
    <!-- menu produtos-->
    <div class="col-xs-12 top5 text-center">
      <ul class="nav nav-pills">
        <li role="presentation" class="active">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos" title="VER TODOS">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-produt1.png" height="19" width="19" alt="">VER TODOS
          </a>
        </li>
        <?php 
          $result = $obj_site->select("tb_categorias_produtos");
          if (mysql_num_rows($result) > 0) {
            while($row = mysql_fetch_array($result)){
            ?>
              <li role="presentation" class="">
                <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                  <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" height="19" width="19"><?php Util::imprime($row[titulo]); ?>
                </a>
              </li>
            <?php 
            }
          }
          ?>
      </ul>

    </div>
    <!-- menu produtos-->
  </div>



  <?php 
  $result = $obj_site->select("tb_produtos", "order by rand() limit 4");
  if (mysql_num_rows($result) > 0) {
    while($row = mysql_fetch_array($result)){
    ?>
        <!-- produto01 -->
        <div class="col-xs-6 produtos-home">
          <div class="thumbnail padding0">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 208, 120, array("class"=>"", "alt"=>"$row[titulo]")) ?>
            </a>
            <div class="caption text-center">
              <h2><?php Util::imprime($row[titulo]); ?></h2>
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn-default btn-transparente-produto top5" role="button">
                SAIBA MAIS
              </a>
            </div>
          </div>
        </div>
        <!-- produto01 -->
    <?php 
    }
  }
  ?>

  

  
</div>
</div>
<!--  ==============================================================  -->
<!-- NOSSOS PRODUTOS -->
<!--  ==============================================================  -->
<?php */ ?>



<!--  ==============================================================  -->
<!-- NOSSOS DICAS HOME -->
<!--  ==============================================================  -->
<div class="container top15 bg-nosso-dicas">
  <div class="row">
    <div class="col-xs-4 text-right padding0">
      <h3>CONFIRA</h3>
      <h2>NOSSAS DICAS</h2>
    </div>

    <div class="col-xs-8">
    <?php 
    $result = $obj_site->select("tb_dicas", "order by rand() limit 4");
    if (mysql_num_rows($result) > 0) {
      while($row = mysql_fetch_array($result)){
      ?>
      <!-- dica 01 -->
      <div class="col-xs-6 dicas-home">
        <div class="thumbnail padding0 dicas-home">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>">
            <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 115, 103, array("class"=>"", "alt"=>"$row[titulo]")) ?>
          </a>
          <div class="caption text-center">
            <p><?php Util::imprime($row[titulo]); ?></p>
          </div>
        </div>
      </div>
      <!-- dica 01 -->
      <?php 
      }
    }
    ?>
    </div>

    

  </div>
</div>  
<!--  ==============================================================  -->
<!-- NOSSOS DICAS HOME -->
<!--  ==============================================================  -->



<?php require_once('./includes/rodape.php'); ?>





</body>

</html>




<?php require_once("./includes/js_css.php"); ?>




  <!-- ---- LAYER SLIDER ---- -->
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/touchcarousel.css"/>
  <link href='<?php echo Util::caminho_projeto() ?>/css?family=Lekton|Lobster' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/black-and-white-skin/black-and-white-skin.css" />
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/jquery.touchcarousel-1.2.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $("#carousel-gallery").touchCarousel({
        itemsPerPage:5,
        scrollbar: false,
        scrollbarAutoHide: true,
        scrollbarTheme: "dark",
        pagingNav: true,
        snapToItems: true,
        scrollToLast: false,
        useWebkit3d: true,
        loopItems: true
      });
    });
  </script>
  <!-- XXXX LAYER SLIDER XXXX -->






