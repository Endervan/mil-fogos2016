<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



<!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  --> 
  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 16) ?>
  <style>
      .bg-interna{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
      }
  </style>
  
</head>



<body class="bg-interna">



	<?php require_once('../includes/topo.php'); ?>



	<!--  ==============================================================  -->
  <!-- DESCRICAO BG -->
  <!--  ==============================================================  -->
  <div class="container img-interna">
    <div class="row">
     <div class="col-xs-12 text-center descricao-dica lato-black">
      <h5><?php Util::imprime($banner[legenda]); ?></h5>
    </div>
  </div>
</div>   

<!--  ==============================================================  -->
<!-- DESCRICAO BG -->
<!--  ==============================================================  -->




<!--  ==============================================================  -->
<!-- Pinterest-like Responsive Grid-->
<!--  ==============================================================  -->
<div class="container ">
  <div class="row">
   <div class="col-xs-12 pintrest">
    
    <?php 
    $result = $obj_site->select("tb_portfolios", $complemento);
    if (mysql_num_rows($result) > 0) {
      $i = 0;
      while($row = mysql_fetch_array($result)){
      ?>
          <!-- dica 01 -->
          <div class="col-xs-6 dicas-home">
            <div class="thumbnail padding0">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/portfolio/<?php Util::imprime($row[url_amigavel]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 129, 116, array("class"=>"", "alt"=>"$row[titulo]")) ?>
              </a>
              <div class="caption text-center">
                <p><?php Util::imprime($row[titulo]); ?></p>
              </div>
            </div>
          </div>
          <!-- dica 01 -->
      <?php 
          if($i == 1){
            echo '<div class="clearfix"></div>';
            $i = 0;
          }else{
            $i++;
          }
      }
    }
    ?>


</div>
</div>
</div>   

<!--  ==============================================================  -->
<!-- Pinterest-like Responsive Grid-->
<!--  ==============================================================  -->







<?php require_once('../includes/rodape.php'); ?>

</body>

</html>






<?php require_once("../includes/js_css.php"); ?>




 <script src="<?php echo Util::caminho_projeto() ?>/jquery/pinterest.js"></script>
 <!-- Pinterest-like Responsive Grid -->
 <script type="text/javascript">
  $(document).ready(function() {
    $('#pinBoot').pinterest_grid({
      no_columns: 4,
      padding_x: 10,
      padding_y: 10,
      margin_bottom: 50,
      single_column_breakpoint: 700
    });
  });
</script>
<!-- Pinterest-like Responsive Grid -->
