<?php 
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>



<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

  

  <!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  --> 
  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 1) ?>
  <style>
      .bg-interna{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
      }
  </style>
  
</head>



<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- DESCRICAO BG -->
  <!--  ==============================================================  -->
  <div class="container top270">
    <div class="row">
     <div class="col-xs-12 text-center">
      <h1><span><?php Util::imprime($banner[legenda]); ?></span></h1>
    </div>
  </div>
</div>   

<!--  ==============================================================  -->
<!-- DESCRICAO BG -->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!-- Pinterest-like Responsive Grid-->
<!--  ==============================================================  -->
<div class="container top270">
  <div class="row">
   <div class="col-xs-12 pintrest">
    <section id="pinBoot">

      <?php 
      $result = $obj_site->select("tb_portfolios");
      if (mysql_num_rows($result) > 0) {
        while ($row = mysql_fetch_array($result)) {
        ?>
            <article class="white-panel">
               <a href="<?php echo Util::caminho_projeto() ?>/portfolio/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" data-toggle="tooltip" data-placement="top" title="<?php Util::imprime($row[titulo]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
              </a>
            </article>
        <?php
        }
      }
      ?>
  </section>
</div>
</div>
</div>   

<!--  ==============================================================  -->
<!-- Pinterest-like Responsive Grid-->
<!--  ==============================================================  -->






<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>

<?php require "includes/js_css.php"; ?>

<script src="<?php echo Util::caminho_projeto() ?>/jquery/pinterest.js"></script>
  <!-- Pinterest-like Responsive Grid -->
  <script type="text/javascript">
    $(document).ready(function() {
      $('#pinBoot').pinterest_grid({
        no_columns: 4,
        padding_x: 10,
        padding_y: 10,
        margin_bottom: 50,
        single_column_breakpoint: 700
      });
    });
  </script>
  <!-- Pinterest-like Responsive Grid -->


