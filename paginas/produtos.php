<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 13);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>



<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



  <!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  -->
  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 7) ?>
  <style>
      .bg-interna{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
      }
  </style>

</head>



<body class="bg-interna">



  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- DESCRICAO BG -->
  <!--  ==============================================================  -->
  <div class="container top270">
    <div class="row">
     <div class="col-xs-12 text-center">
      <h1><span><?php Util::imprime($banner[legenda]); ?></span></h1>
    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!-- DESCRICAO BG -->
<!--  ==============================================================  -->




<!--  ==============================================================  -->
<!-- NOSSOS PRODUTOS -->
<!--  ==============================================================  -->

<div class="container top100">
  <div class="row">
    <div class="fundo-azul">

      <!-- menu produtos-->
      <div class="col-xs-12 padding0 bottom20 top18">
        <ul class="nav nav-pills">

          <li class="col-xs-2" role="presentation" class="<?php if(Url::getURL(1) == ''){ echo 'active'; } ?> ">
            <a href="<?php echo Util::caminho_projeto() ?>/produtos" title="VER TODOS">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-produt1.png" class="right10" alt="">VER TODOS
            </a>
          </li>


            <?php
                $result = $obj_site->select("tb_categorias_produtos","order by rand() limit 4");
                if (mysql_num_rows($result) > 0) {
                  while($row = mysql_fetch_array($result)){
                  ?>
                    <li class="col-xs-2 text-center padding0" role="presentation" class="<?php if(Url::getURL(1) == $row[url_amigavel]){ echo 'active'; } ?> ">
                      <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                        <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" class="right5" alt=""><?php Util::imprime($row[titulo]); ?>
                      </a>
                    </li>
                  <?php
                  }
                }
                ?>



          <div class="top2 col-xs-2 div_personalizada bottom10">
            <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
              <div class="input-group input-group-lg">
                <input type="text" name="busca_topo" class="form-control form" placeholder="BUSCAR PRODUTOS">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                </span>
              </div>
            </form>
        </div>
      </ul>

    </div>
    <!-- menu produtos-->
  </div>




  <?php
  $url1 = Url::getURL(1);

   //  FILTRA AS CATEGORIAS
   if (isset( $url1 )) {
      $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url1);
      $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
   }

   //  FILTRA PELO TITULO
   if(isset($_POST[busca_topo])):
     $complemento = "AND titulo LIKE '%$_POST[busca_topo]%'";
   endif;



  $result = $obj_site->select("tb_produtos", $complemento);
  if(mysql_num_rows($result) == 0){
     echo "<h2 class='bg-info' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
  }else{
    $i = 0;
    while($row = mysql_fetch_array($result)){
    ?>
      <!-- produto01 -->
      <div class="col-xs-3 produtos-home top10">
        <div class="thumbnail padding0">
         <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
            <div class="sombra-prod"> </div>
            <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 263, 155, array("class"=>"", "alt"=>"$row[titulo]")) ?>
         </a>
          <div class="caption text-center">
            <h2><?php Util::imprime($row[titulo]); ?></h2>
            <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn-default btn-transparente-produto  top30" role="button" title="SAIBA MAIS">SAIBA MAIS</a>
          </div>
        </div>
      </div>
      <!-- produto01 -->
    <?php
      if($i == 3){
          echo '<div class="clearfix"></div>';
          $i = 0;
      }else{
        $i++;
      }
    }
  }
  ?>




</div>
</div>
<!--  ==============================================================  -->
<!-- NOSSOS PRODUTOS -->
<!--  ================================================================-->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
<?php require "includes/js_css.php"; ?>
