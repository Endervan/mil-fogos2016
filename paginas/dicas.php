<?php 
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 10);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

  
  <!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  --> 
  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 3) ?>
  <style>
      .bg-interna{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
      }
  </style>

</head>



<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- DESCRICAO BG -->
  <!--  ==============================================================  -->
  <div class="container top270">
    <div class="row">
     <div class="col-xs-12 text-center portfolio-dentro">
      <h1><span><?php Util::imprime($banner[legenda]); ?></span></h1>
    </div>
  </div>
</div>   
<!--  ==============================================================  -->
<!-- DESCRICAO BG -->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!--   barra pesquisas e categorias -->
<!--  ==============================================================  -->
<div class="container top120">
  <div class="row">
   <div class="fundo-cinza effect2">
    <div class="col-xs-3 top15">
      <h5><span>BUSCAR PRODUTO:</span></h5>
    </div>
    
  <div class="col-xs-9 top15">
    <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
       <div class="input-group input-group-lg">
        <input type="text" class="form-control form" name="busca_topo" placeholder="O QUE VOCÊ ESTÁ PROCURANDO?">
        <span class="input-group-btn">
          <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
        </span>
      </div>
    </form>       
</div>

</div>
</div>
</div>
<!--  ==============================================================  -->
<!--   barra pesquisas e categorias -->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!-- NOSSOS DICAS HOME -->
<!--  ==============================================================  -->
<div class="container top30 nossos-dicas">
  <div class="row">



        <?php 
        $result = $obj_site->select("tb_dicas");
        if (mysql_num_rows($result) > 0) {
          $i = 0;
          while($row = mysql_fetch_array($result)){
          ?>
            <!-- dica 01 -->
            <div class="col-xs-3 dicas-home">
              <div class="thumbnail padding0">
                <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>">
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 263, 233, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                </a>
                <div class="caption text-center">
                  <p><?php Util::imprime($row[titulo]); ?></p>
                </div>
              </div>
            </div>
            <!-- dica 01 -->        
          <?php 
              if($i == 3){
                echo '<div class="clearfix"></div>';
                $i = 0;
              }else{
                $i++;
              }
          }
        }
        ?>
    


  </div>
</div>  

<!--  ==============================================================  -->
<!-- NOSSOS DICAS HOME -->
<!--  ==============================================================  -->



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require "includes/js_css.php"; ?>