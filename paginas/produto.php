<?php  


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
   $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/produtos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>




  <!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  --> 
  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 8) ?>
  <style>
      .bg-interna{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
      }
  </style>

</head>



<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- DESCRICAO BG -->
  <!--  ==============================================================  -->
  <div class="container top270">
    <div class="row">
     <div class="col-xs-12 text-center">
      <h1><span><?php Util::imprime($banner[legenda]); ?></span></h1>
    </div>
  </div>
</div>   

<!--  ==============================================================  -->
<!-- DESCRICAO BG -->
<!--  ==============================================================  -->




<!--  ==============================================================  -->
<!--  PRODUTOS DENTRO DESCRICAO-->
<!--  ==============================================================  -->
<div class="container top200">
  <div class="row produto-dentro">
    <div class="col-xs-6">
      <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]", 573, 358, array("class"=>"", "alt"=>"$dados_dentro[titulo]")) ?>
    </div>
    <div class="col-xs-6">
      <h1><?php Util::imprime($dados_dentro[titulo]); ?></h1>
      <div class="top20">
        <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
      </div>
      
      <?php if(!empty($dados_dentro[preco])): ?>
      <div class="top20">
        <p><strong>Valor:</strong> R$ <?php echo Util::formata_moeda($dados_dentro[preco]); ?></p>
      </div>
      <?php endif; ?>


      <a class="btn btn-roxo top20" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">
        SOLICITE UM ORÇAMENTO
      </a>


    </div>

    <div class="clearfix "></div>




    <!-- descricao video -->
    <?php if (!empty($dados_dentro[src_youtube])): ?>
        <div class="col-xs-5 text-right top100">
          <h5><span>CONFIRA O VÍDEO DE APRESENTAÇÃO DO PRODUTO</span></h5>
         <div class="top20">
            <p><?php Util::imprime($dados_dentro[descricao_video]); ?></p>
          </div>
        </div>
        <!--  video -->
        <div class="col-xs-7 top70">
          <iframe width="100%" height="410" src="<?php Util::imprime($dados_dentro[src_youtube]); ?>" frameborder="0" allowfullscreen></iframe>
       </div>
   <?php endif ?>
   <!-- descricao video -->




  </div>
</div>
<!--  ==============================================================  -->
<!--  PRODUTOS DENTRO DESCRICAO-->
<!--  ==============================================================  -->




<!--  ==============================================================  -->
<!-- veja tambem-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row">
    <div class="col-xs-12 top100 bottom30">
      <h3>VEJA TAMBÉM</h3>
    </div>

    <?php
    $result = $obj_site->select("tb_produtos", "order by rand() limit 4");
    if(mysql_num_rows($result) > 0){
      while($row = mysql_fetch_array($result)){
      ?>
        <!-- produto01 -->
        <div class="col-xs-3 produtos-home top10">
          <div class="thumbnail padding0">
           <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <div class="sombra-prod"> </div>
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 263, 155, array("class"=>"", "alt"=>"$row[titulo]")) ?>
           </a>
            <div class="caption text-center">
              <h2><?php Util::imprime($row[titulo]); ?></h2>
              <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn-default btn-transparente-produto  top30" role="button" title="SAIBA MAIS">SAIBA MAIS</a>
            </div>
          </div>
        </div>
        <!-- produto01 -->
      <?php 
      }
    }
    ?>




</div>
</div> 
<!--  ==============================================================  -->
<!-- veja tambem-->
<!--  ==============================================================  --> 


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
<?php require "includes/js_css.php"; ?>