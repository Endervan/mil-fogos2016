<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 11);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



<!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  -->
  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 5) ?>
  <style>
      .bg-interna{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
      }
  </style>

</head>



<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- DESCRICAO BG -->
  <!--  ==============================================================  -->
  <div class="container top350">
    <div class="row">
     <div class="col-xs-12 text-center">
      <h1><span><?php Util::imprime($banner[legenda]); ?></span></h1>
    </div>
  </div>
</div>

<!--  ==============================================================  -->
<!-- DESCRICAO BG -->
<!--  ==============================================================  -->




<!--  ==============================================================  -->
<!-- FORMULARIO CONTATOS E ENDERENCO-->
<!--  ==============================================================  -->
<div class="container top170">
  <div class="row bottom20 contatos">
    <div class="col-xs-3 col-xs-offset-2">
      <!-- Nav tabs -->
      <ul class="nav nav-stacked" role="tablist">
        <li role="presentation" class="active fale-conosco"><a href="javascript:void(0);" aria-controls="home" role="tab" data-toggle="tab"><img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-fale.png" alt="">FALE CONOSCO</a>
        </li>
        <li  class="trabalhe-conosco" role="presentation"><a href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco"><img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-trabalhe.png" alt="">TRABALHE CONOSCO</a></li>
      </ul>
    </div>

    <div class="clearfix"></div>






    <!-- contatos -->
  <div class="col-xs-5 top90">









  <?php
  $result = $obj_site->select("tb_lojas");
  if (mysql_num_rows($result) > 0) {
    while($row = mysql_fetch_array($result)){
    ?>
        <div class="col-xs-5 col-xs-offset-7 top20 text-center">
           <div class="fundo-contatos">
             <h1><?php Util::imprime($row[titulo]); ?></h1>
           </div>
         </div>


         <div class="col-xs-12">
          <div class="fundo-contatos">
            <div class="media">
              <div class="media-left media-middle">
                  <img class="media-object left10" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-telefone.png" alt="">
              </div>
              <div class="media-body media-middle">
                <div class="pull-left top15">
                  <h5 class="media-heading"><?php Util::imprime($row[telefone]); ?></h5>
                </div>
                <div class="pull-right top5">
                  <?php if (!empty($row[link_maps])): ?>
                      <a href="<?php Util::imprime($row[link_maps]); ?>" title="COMO CHEGAR" class="btn btn-azul-claro left10" target="_blank">
                        COMO CHEGAR
                      </a>
                  <?php endif ?>
               </div>
             </div>
           </div>

           <div class="media bottom15 top15">
            <div class="media-left media-middle">
                <img class="media-object left10" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-enderenco.png" alt="">
            </div>
            <div class="media-body media-middle ">
             <p><?php Util::imprime($row[endereco]); ?></p>
           </div>
         </div>
        </div>
        </div>
    <?php
    }
  }
  ?>















</div>
<!-- contatos -->






<!-- Tab panes -->
<div class="tab-content">

  <!-- fale conosco -->
  <div role="tabpanel" class="tab-pane fade in active" id="home">




    <form class="form-inline FormContato" role="form" method="post" enctype="multipart/form-data">
      <div class="top50 bottom25">

        <div class="col-xs-7 fundo-formulario">
          <!-- formulario orcamento -->
          <div class="top20">
            <div class="top15">
              <div class="col-xs-6 form-group ">
                <label class="fa fa-user"> <span class="left5">Nome</span></label>
                <input type="text" name="nome" class="form-control fundo-form1 input100" placeholder="">
              </div>

              <div class="col-xs-6 form-group ">
                <label class="fa fa-envelope"> <span class="left5">E-mail</span></label>
                <input type="text" name="email" class="form-control fundo-form1 input100" placeholder="">
              </div>
            </div>


            <div class="clearfix"></div>
            <div class="top15">
              <div class="col-xs-6 form-group">
                <label class="fa fa-phone"> <span class="left5">Telefone</span></label>
                <input type="text" name="telefone" class="form-control fundo-form1 input100" placeholder="">
              </div>

              <div class="col-xs-6 form-group">
               <label class="fa fa-star"> <span class="left5">Assunto</span></label>
               <input type="text" name="assunto" class="form-control fundo-form1 input100" placeholder="">
             </div>
           </div>


           <div class="clearfix"></div>
           <div class="top15">
            <div class="col-xs-12 form-group">
              <label class="fa fa-pencil"> <span class="left5">Sua Mensagem</span></label>
              <textarea name="mensagem" id="" cols="30" rows="16" class="form-control  fundo-form1 input100" placeholder=""></textarea>
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="col-xs-12">

            <div class="pull-right  top30 bottom25">
              <button type="submit" class="btn btn-formulario" name="btn_contato">
                ENVIAR
              </button>
            </div>
          </div>


        </div>
        <!-- formulario orcamento -->

      </div>

    </div>
  </form>



</div>
<!-- fale conosco -->


</div>
<!-- Tab panes -->

</div>
</div>
<!--  ==============================================================  -->
<!-- FORMULARIO CONTATOS E ENDERENCO-->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!-- mapa -->
<!--  ==============================================================  -->
<div class="container top60">
  <div class="row">
    <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="800" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>
</div>


<!--  ==============================================================  -->
<!-- mapa -->
<!--  ==============================================================  -->





<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>






<?php require "includes/js_css.php"; ?>




<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      cargo: {
        validators: {
          notEmpty: {

          }
        }
      },
      area: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
  });
</script>







<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[btn_contato]))
{
  $nome_remetente = $_POST[nome];
  $email = $_POST[email];
  $assunto = $_POST[assunto];
  $telefone = $_POST[telefone];
  $mensagem = nl2br($_POST[mensagem]);
  $texto_mensagem = "
					Nome: $nome_remetente <br />
					Assunto: $assunto <br />
					Telefone: $telefone <br />
					Email: $email <br />
					Mensagem: <br />
					$mensagem
					";

  Util::envia_email($config[email], "CONTATO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, $nome_remetente, $email);
  Util::envia_email($config[email_copia], "CONTATO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, $nome_remetente, $email);
  Util::alert_bootstrap("Obrigado por entrar em contato.");
  unset($_POST);
}
?>
