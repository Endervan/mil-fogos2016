<?php 


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 14);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];



//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
    //  SELECIONO O TIPO
    switch($_GET[tipo])
    {
        case "produto":
            $id = $_GET[id];
            unset($_SESSION[solicitacoes_produtos][$id]);
            sort($_SESSION[solicitacoes_produtos]);
        break;
        case "servico":
            $id = $_GET[id];
            unset($_SESSION[solicitacoes_servicos][$id]);
            sort($_SESSION[solicitacoes_servicos]);
        break;
        case "piscina_vinil":
            $id = $_GET[id];
            unset($_SESSION[piscina_vinil][$id]);
            sort($_SESSION[piscina_vinil]);
        break;
    }

}
?>



<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>




  <!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  --> 
  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 9) ?>
  <style>
      .bg-interna{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
      }
  </style>

</head>



<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->

=

  <!--  ==============================================================  -->
  <!-- DESCRICAO BG -->
  <!--  ==============================================================  -->
  <div class="container top270">
    <div class="row">
     <div class="col-xs-12 text-center">
      <h1><span><?php Util::imprime($banner[legenda]); ?></span></h1>
    </div>
  </div>
</div>   

<!--  ==============================================================  -->
<!-- DESCRICAO BG -->
<!--  ==============================================================  -->




















<form class="form-inline FormContato" role="form" method="post">



<!--  ==============================================================  -->
<!-- carrinho e formulario -->
<!--  ==============================================================  -->
<div class="container top40">
  <div class="row top170">

    <div class="col-xs-6 tb-lista-itens padding0">
      <h5 class="bottom30">PRODUTOS SELECIONADOS (<?php echo count($_SESSION[solicitacoes_produtos]) ?>)</h5>
     
         <table class="table">
            <tbody>

          <?php
          if(count($_SESSION[solicitacoes_produtos]) > 0){
            for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
              $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
            ?>
              
                    <tr>
                      <td>
                        <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 153, 92, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                      </td>
                      <td align="left"><?php Util::imprime($row[titulo]) ?></td>
                      <td class="">
                          <input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                          <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                      </td>
                      <td class="text-center">
                        <a href="?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir">
                          <i class="fa fa-times-circle fa-2x"></i>
                        </a>
                      </td>
                    </tr>
                  
            <?php 
            }
          }
          ?>
            </tbody>
          </table>

          

      <div class=" top15">

        <a href="<?php echo Util::caminho_projeto() ?>/produtos" title="Continuar orçando" class="btn btn-azul-claro">
          Continuar orçando
        </a>
      </div>

    </div>



    <div class="col-xs-6 formulario-orcamento bottom30">

      <h5 class="text-center bottom10">CONFIRME SEUS DADOS</h5>

      


        <div class="titulo-form-orcamento">

          <!-- form fale conosco -->
          <div class="col-xs-6 form-group">
            <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
            <input type="text" name="nome" class="form-control input100" placeholder="">
          </div>
          <div class="col-xs-6 form-group">
            <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
            <input type="text" name="email" class="form-control input100" placeholder="">
          </div>

          <div class="clearfix"></div>

          <div class="col-xs-6 top20 form-group">
            <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
            <input type="text" name="telefone" class="form-control input100" placeholder="">
          </div>

          <div class="col-xs-6 top20 form-group">
            <label class="glyphicon glyphicon-map-marker"> <span>Celular</span></label>
            <input type="text" name="celular" class="form-control input100" placeholder="">
          </div>

          

          <div class="clearfix"></div>

          <div class="col-xs-6 top20 form-group">
            <label class="glyphicon glyphicon-map-marker"> <span>Cidade</span></label>
            <input type="text" name="cidade" class="form-control input100" placeholder="">
          </div>
          <div class="col-xs-6 top20 form-group">
            <label class="glyphicon glyphicon-map-marker"> <span>Bairro</span></label>
            <input type="text" name="bairro" class="form-control input100" placeholder="">
          </div>

          <div class="clearfix"></div>
          

          <div class="col-xs-6 top20 form-group">
            <label class="glyphicon glyphicon-map-marker"> <span>Estado</span></label>
            <input type="text" name="estado" class="form-control input100" placeholder="">
          </div>
          
          <div class="col-xs-6 top20 form-group">
            <label class="glyphicon glyphicon-map-marker"> <span>Cep</span></label>
            <input type="text" name="cep" class="form-control input100" placeholder="">
          </div>

          <div class="clearfix"></div>

          <div class="col-xs-12 top20 form-group">
            <label class="glyphicon glyphicon-pencil"> <span>Mensagem</span></label>
            <textarea name="mensagem" id="" cols="30" rows="10" class="form-control input100"></textarea>
          </div>

          <div class="clearfix"></div>

          <!-- perguntas frequentes -->
          <div class="top30">
            <p>Tem interesse em receber orçamento de outros produtos:</p>

            <label class="radio-inline">
              <input type="radio" name="receber_orcamento" id="inlineRadio1" value="SIM"> SIM
            </label>
            <label class="radio-inline">
              <input type="radio" name="receber_orcamento" id="inlineRadio2" value="NÃO"> NÃO
            </label>
            <p class="top25">Como conheçeu nosso site?</p>

           
          </div>
          <!-- perguntas frequentes -->

            <div class=" col-xs-8 ">
              <p class="top20"></p>
              <select class="form-control input100 top5 input-lg" name="como_conheceu" >
            <option value="">Selecione</option>
            <option>Google</option>
            <option>Jornais</option>
            <option>Revistas</option>
            <option>Sites</option>
            <option>Fórum</option>
            <option>Notícias</option>
            <option>Outros</option>
          </select>
            </div>       
            <div class="col-xs-4 top20">
              <button type="submit" class="btn btn-default btn-formulario pull-right">ENVIAR</button>
            </div>
          

          <!-- form fale conosco -->
        </div>
     

    </div>


    
  </div>
</div>
<!--  ==============================================================  -->
<!-- carrinho e formulario -->
<!--  ==============================================================  -->



</form>



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>

<?php require "includes/js_css.php"; ?>

<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          },
          phone: {
            country: 'BR',
            message: 'O telefone %s não é válido'
          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      bairro: {
        validators: {
          notEmpty: {

          }
        }
      },
      complemento: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>







<?php
//  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
if(isset($_POST[nome])){

    //  CADASTRO OS PRODUTOS SOLICITADOS
    for($i=0; $i < count($_POST[qtd]); $i++){
        $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

        $mensagem .= "
                    <tr>
                        <td><p>". $_POST[qtd][$i] ."</p></td>
                        <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                     </tr>
                    ";
    }


    if (count($_POST[categorias]) > 0) {
        foreach($_POST[categorias] as $cat){
            $desc_cat .= $cat . ' , ';
        }
    }


    //  ENVIANDO A MENSAGEM PARA O CLIENTE
    $texto_mensagem = "
                        O seguinte cliente fez uma solicitação pelo site. <br />

                        Nome: $_POST[nome] <br />
                        Email: $_POST[email] <br />
                        Telefone: $_POST[telefone] <br />
                        Celular: $_POST[celular] <br />
                        Bairro: $_POST[bairro] <br />
                        Cidade: $_POST[cidade] <br />
                        Estado: $_POST[estado] <br />
                        Receber orçamento: $_POST[receber_orcamento] <br />
                        Categorias desejadas: $desc_cat <br />
                        Como conheceu nosso site: $_POST[como_conheceu] <br />

                        Mensagem: <br />
                        ". nl2br($_POST[mensagem]) ." <br />

                        <br />
                        <h2> Produtos selecionados:</h2> <br />

                        <table width='100%' border='0' cellpadding='5' cellspacing='5'>
                            <tr>
                                  <td><h4>QTD</h4></td>
                                  <td><h4>PRODUTO</h4></td>
                            </tr>
                            $mensagem
                        </table>

                        ";

   

    Util::envia_email($config[email], "ORÇAMENTO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, $nome_remetente, $email);
    Util::envia_email($config[email_copia], "ORÇAMENTO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, $nome_remetente, $email);
    unset($_SESSION[solicitacoes_produtos]);
    unset($_SESSION[solicitacoes_servicos]);
    unset($_SESSION[piscinas_vinil]);
    Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

}
?>
